package com.grocico.grochery.extra;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.grocico.grochery.R;
import com.grocico.grochery.activity.CartActivity;


public class MyRadioButton implements View.OnClickListener{

    private TextView iv;
    private TextView tv;
    private RadioButton rb;
    private View view;

    public MyRadioButton(Context context) {
        view = View.inflate(context, R.layout.my_radio_button, null);
        rb =view.findViewById(R.id.radioButton1);
        tv =  view.findViewById(R.id.tv1);
        iv = view.findViewById(R.id.tv2);
        view.setOnClickListener(this);
        rb.setOnCheckedChangeListener(null);
    }


    public View getView() {
        return view;
    }

    @Override
    public void onClick(View v) {
        boolean nextState = !rb.isChecked();
        if (!rb.isChecked()){
            LinearLayout lGroup = (LinearLayout)view.getParent();
            if(lGroup != null){
                int child = lGroup.getChildCount();
                for(int i=0; i<child; i++){
                    //uncheck all
                    ((RadioButton)lGroup.getChildAt(i).findViewById(R.id.radioButton1)).setChecked(false);
                }
            }
            rb.setChecked(nextState);
            for (int i =0; i < lGroup.getChildCount(); i++){
                if (((RadioButton)lGroup.getChildAt(i).findViewById(R.id.radioButton1)).isChecked()){
                    Constant.paymentMethod = i;
                    if (Constant.paymentMethod==3){
                        CartActivity.TotalAmount.setText("₹"+Math.round(CartActivity.totalAmount+Integer.valueOf(Constant.deliveryCharge)- Constant.useableReferralAmount+Math.round((CartActivity.totalAmount*8)/100)));
                    }else {
                        CartActivity.TotalAmount.setText("₹"+Math.round(CartActivity.totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
                    }
                }
            }

        }

    }

    public void setEnabled(Boolean b){
        view.setEnabled(b);
        view.setClickable(b);
        rb.setEnabled(b);

    }

    public void setSecondaryText(String b){
        iv.setText(b);
    }

    public void setText(String text){
        tv.setText(text);
    }

    public void setChecked(boolean isChecked){
        rb.setChecked(isChecked);
    }


}
