package com.grocico.grochery.extra;

import android.content.Context;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


public class Constant {
    public static String BASE_URL = "http://grocico.in/app/";
    public static DatabaseReference rootRef,schoolListRef,requestBookRef,updateUrlRef;
    public static String cityName="jabalpur";
    public static String searchCityName="jabalpur";
    public static String stateName;
    public static String landMark;
    public static String countryName;
    public static String postalCode;
    public static String uid;
    public static String userName;
    public static String userPhone;
    public static String userEmail;
    public static String userAddress;
    public static String userToken;
    public static String userLat;
    public static String userLng;


    public static int maxOrderQuantity = 4;
    public static String CartItemCount="0";
    public static String deliveryCharge="0";
    public static String minLimitDeliveryCharge="0";
    public static String totalPayableAmount="0";
    public static int paymentMethod = -1;
    public static DatabaseReference historyRef;
    public static int referralAmount = 0;
    public static int useableReferralAmount = 0;
    public static int breaker= 0;
    public static int interestAmount = 0;
    public static String isPromoCodeApplied ="false",promoCodeApplied=null;
    public static String searchKey;
    public static String mid="gqntcE68709721515545";

    private OnResponseFromServer onResponseFromServer;
    public static String upperCaseWords(String sentence) {
        String words[] = sentence.replaceAll("\\s+", " ").trim().split(" ");
        String newSentence = "";
        for (String word : words) {
            for (int i = 0; i < word.length(); i++)
                newSentence = newSentence + ((i == 0) ? word.substring(i, i + 1).toUpperCase():
                        (i != word.length() - 1) ? word.substring(i, i + 1).toLowerCase() : word.substring(i, i + 1).toLowerCase().toLowerCase() + " ");
        }

        return newSentence;
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

//    public void geCartItemCount(Context ctx){
//        HashMap hm = new HashMap();
//        hm.put("uid",Constant.uid);
//            getDataFromServer(ctx, Request.Method.POST, "getCartItemCount", new OnResponseFromServer() {
//            @Override
//            public void onEvent(String response) {
//                try {
//                    JSONArray jsonArray = new JSONArray(response);
//                    Constant.CartItemCount = jsonArray.getJSONObject(0).getString("count");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        },hm);
//    }



    public interface OnResponseFromServer{
        void onEvent(String response);
    }

    public void getDataFromServer(Context ctx, int type, String path, OnResponseFromServer eventListener, final HashMap hm){
        onResponseFromServer=eventListener;
        RequestQueue queue = Volley.newRequestQueue(ctx);
        StringRequest stringRequest = new StringRequest(type, Constant.BASE_URL + path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent(response);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent("error");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE:>>> ", error.toString() + "<<<");
                if(onResponseFromServer!=null){
                    onResponseFromServer.onEvent("error");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return hm;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }
        };

        queue.add(stringRequest);

    }



}
