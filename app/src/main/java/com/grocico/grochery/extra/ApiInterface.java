package com.grocico.grochery.extra;

import com.grocico.grochery.model.CartItemModel;
import com.grocico.grochery.model.HomeInfiniteModel;
import com.grocico.grochery.model.ItemsGridModel;
import com.grocico.grochery.model.OrderModel;
import com.grocico.grochery.model.PurchaseDetailModel;
import com.grocico.grochery.model.SearchResultModel;
import com.grocico.grochery.model.ShopDetailsModel;
import com.grocico.grochery.model.subCategoryModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("getSubCatByCatName")
    Call<List<subCategoryModel>> getSubCategory(@Query("category_name") String categoryName,@Query("city") String city);

    @GET("getItemsBySubCat")
    Call<List<ItemsGridModel>> getItems(@Query("category_name") String category_name, @Query("subcategory") String subcategory,@Query("city") String city);

    @GET("getItemsByCategory")
    Call<List<ItemsGridModel>> getItemsByCat(@Query("category_name") String category_name,@Query("city") String city);

    @GET("getHomeItemsByCategory")
    Call<List<ItemsGridModel>> getHomeItemsByCategory(@Query("category_name") String category_name,@Query("city") String city);

    @GET("getCartItem")
    Call<ArrayList<CartItemModel>> getCartItem(@Query("uid") String category_name);

    @GET("getUserOrder")
    Call<ArrayList<OrderModel>> getUserOrder(@Query("uid") String uid);

    @GET("getPurchaseFromOrder")
    Call<ArrayList<PurchaseDetailModel>> getPurchaseFromOrder(@Query("order_id") String order_id);

    @GET("getAllShopDetails")
    Call<ArrayList<ShopDetailsModel>> getAllShopDetails(@Query("city") String city);

    @GET("getItemByShopCode")
    Call<List<ItemsGridModel>> getItemByShopCode(@Query("shopCode") String shopCode);

    @GET("getSearch")
    Call<List<SearchResultModel>> getSearch(@Query("searchKey") String searchKey);


    @GET("getSearchResult")
    Call<List<ItemsGridModel>> getSearchResult(@Query("searchKey") String searchKey,@Query("city") String city);

    @POST("getAllBestSeller")
    Call<List<ItemsGridModel>> getAllBestSeller(@Query("city") String city);

    @POST("getBestSeller")
    Call<List<ItemsGridModel>> getBestSeller(@Query("city") String city);

    @POST("getNewArrival")
    Call<List<ItemsGridModel>> getNewArrival(@Query("city") String city);

    @POST("getAllNewArrival")
    Call<List<ItemsGridModel>> getAllNewArrival(@Query("city") String city);

    @GET("getHomeItems")
    Call<List<HomeInfiniteModel>> getHomeInfiniteMenu();


}
