package com.grocico.grochery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.grocico.grochery.R;
import com.grocico.grochery.activity.CartActivity;
import com.grocico.grochery.activity.MainActivity;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.extra.MyRadioButton;


public class PaymentFragment extends Fragment {
    String[] st1 = {"Google Pay","Paytm","Cash On Delivery"};
    String[] st2 = {"pay your amount with Google Pay","pay your amount with Paytm Wallet","pay your amount after the delivery"};
    TextView namePostalCode,address,phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_payment, container, false);


        LinearLayout linearLayout = v.findViewById(R.id.linearLayoutGroup);

        namePostalCode=v.findViewById(R.id.name_postal_code);
        address = v.findViewById(R.id.address);
        phone = v.findViewById(R.id.phone);

        namePostalCode.setText(Constant.userName+", "+Constant.postalCode);
        address.setText(Constant.userAddress);
        phone.setText(Constant.userPhone);

        TextView total_item = v.findViewById(R.id.total_item);
        total_item.setText("Price ("+ Constant.CartItemCount+" item)");
        TextView total_price = v.findViewById(R.id.total_price);
        total_price.setText("₹"+ String.valueOf((int) CartActivity.totalAmount));
        LinearLayout ll = v.findViewById(R.id.referral_lay);
        TextView item_name = v.findViewById(R.id.referral_amount);
        if ((int) CartActivity.totalAmount!=0 && (int) CartActivity.totalAmount>=250 && Constant.referralAmount>=10){
            ll.setVisibility(View.VISIBLE);
            item_name.setText("- ₹"+((Constant.referralAmount*20)/100));
            item_name.setTextColor(getContext().getResources().getColor(R.color.red));
            Constant.useableReferralAmount = ((Constant.referralAmount*20)/100);
        }else {
            ll.setVisibility(View.GONE);
            item_name.setText("₹"+0);
        }

        TextView total_amount_payable = v.findViewById(R.id.total_amount_payable);
        total_amount_payable.setText("₹"+ String.valueOf((int)CartActivity.totalAmount+ Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
        TextView delivery_charge = v.findViewById(R.id.delivery_charge);
        delivery_charge.setText("₹"+Constant.deliveryCharge);
        TextView save_txt = v.findViewById(R.id.save_txt);
        save_txt.setText("You will save total ₹"+ String.valueOf(((int)CartActivity.totalActualAmount-CartActivity.totalAmount)+Constant.useableReferralAmount)+" on this order");



        for(int i=0; i<st1.length; i++){
            MyRadioButton mrb = new MyRadioButton(getContext());

            mrb.setText(st1[i]);
            mrb.setSecondaryText(st2[i]);
            if (i == 3 && CartActivity.totalAmount >2500 && MainActivity.bookSetList.size()!=0){
                mrb.setEnabled(true);
                mrb.setSecondaryText("4 Months × ₹"+Math.round(CartActivity.totalAmount/4) + " + ₹"+Math.round((CartActivity.totalAmount*8)/100)+"(8%) = ₹"+Math.round(CartActivity.totalAmount+((CartActivity.totalAmount*8)/100)));
            }else if (i == 3){
                mrb.setEnabled(false);
            }
            linearLayout.addView(mrb.getView());
        }

        return v;
    }

}
