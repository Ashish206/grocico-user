package com.grocico.grochery.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.activity.SchoolSetAllDetails;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.AllSchool;
import com.grocico.grochery.model.EditAvailableClassModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;


public class SchoolListFragment extends Fragment {
    FloatingActionButton addFab;
    ProgressDialog progressDialog;
    RecyclerView itemList;
    public static ArrayList classAvailable;
    String[] className = {"Nursery","KG1","KG2","Class 1","Class2","Class 3","Class 4","Class 5","Class 6","Class 7","Class 8","Class 9","Class 10",
            "Class 11th(Science)","Class 11(Commerce)","Class 11(Arts)","Class 11(Humanities)","Class 12th(Science)","Class 12(Commerce)","Class 12(Arts)","Class 12(Humanities)"};

    @SuppressLint("RestrictedApi")


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_school_list, container, false);

        addFab = v.findViewById(R.id.add_fab_btn);
            addFab.setVisibility(View.GONE);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog();
            }
        });

        classAvailable = new ArrayList();

        itemList = v.findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));


        return v;
    }



    private void showAddItemDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Item");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View editDialog = inflater.inflate(R.layout.add_school_dialog,null);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final TextInputEditText pageCountEt = editDialog.findViewById(R.id.schoolAddress);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        isCopyAvailable.setChecked(true);
        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) && !TextUtils.isEmpty(pageCountEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("SchoolName",copyTypeEt.getText().toString());
                    hm.put("Address",pageCountEt.getText().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }

                    String key =  Constant.schoolListRef.push().getKey();
                    Constant.schoolListRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "School Info Edited!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getContext(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onStart() {
        super.onStart();
        refreshBookRecyclerView();
    }

    private void refreshBookRecyclerView() {
        FirebaseRecyclerOptions<AllSchool> options =
                new FirebaseRecyclerOptions.Builder<AllSchool>()
                        .setQuery(Constant.schoolListRef.orderByChild("SchoolName"), AllSchool.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<AllSchool, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_all_school_list_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final AllSchool model) {
                holder.setSchoolName(model.getSchoolName());
                holder.setSchoolImage(model.getPhone(),getContext());
               // holder.setPhone(model.getPhone());
//                holder.mView.findViewById(R.id.phone_number).setVisibility(View.GONE);
//                holder.setAlphabetImage(model.getSchoolName().substring(0, 1));
                final String id =getRef(position).getKey();





                    holder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (model.getSetAvailable().equals("true")){

                                loadAvailableClass(id,model.getSchoolName());

                            //showClassList(id,model.getSchoolName());
                            }else {
                                Toast.makeText(getContext(), "Set Not Available", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                        holder.mView.findViewById(R.id.menu_btn).setVisibility(View.GONE);


            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private void showEditAvailableClassDialog(String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle("Edit Available Class");
        LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.edit_available_class_dialog,null,false);
        builder.setView(v);
        RecyclerView list = v.findViewById(R.id.class_list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        loadEditAvailableClass(id,list);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
              dialogInterface.dismiss();
            }
        });
        builder.show();


    }

    private void loadEditAvailableClass(final String schoolId, RecyclerView classList) {
        FirebaseRecyclerOptions<EditAvailableClassModel> options =
                new FirebaseRecyclerOptions.Builder<EditAvailableClassModel>()
                        .setQuery(Constant.schoolListRef.child(schoolId).child("ClassAvailableList"), EditAvailableClassModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<EditAvailableClassModel, classListViewHolder>(options) {
            @Override
            public classListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.edit_available_class_dialog_single_list, parent, false);

                return new classListViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(classListViewHolder holder, int position, final EditAvailableClassModel model) {
                holder.setClassName(getRef(position).getKey().replace("_"," "));
                holder.setIsAvailable(model.getIsAvailable(),getRef(position).getKey(),schoolId);

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();

            }
        };
        adapter.startListening();
        classList.setAdapter(adapter);
    }

    class classListViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public classListViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        private void setIsAvailable(String s, final String key, final String schoolId){
            CheckBox cb = mView.findViewById(R.id.cb);
            if (s.equals("true")){
                cb.setChecked(true);
            }else {
                cb.setChecked(false);
            }


            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(((CompoundButton) view).isChecked()){
                        Constant.schoolListRef.child(schoolId).child("ClassAvailableList").child(key).child("isAvailable").setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getContext(), "Opps:/"+task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        Constant.schoolListRef.child(schoolId).child("ClassAvailableList").child(key).child("isAvailable").setValue("false").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getContext(), "Opps:/"+task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            });

        }

        private void setClassName(String name)
        {
            TextView userNameView= mView.findViewById(R.id.class_name);
            userNameView.setText(name);

        }

    }

    private void loadAvailableClass(final String id, final String schoolName) {
        classAvailable.clear();
        Constant.schoolListRef.child(id).child("ClassAvailableList").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot ds : dataSnapshot.getChildren()){
                    if (ds.child("isAvailable").getValue().equals("true")){
                        if (ds.getKey().equals("Class_2")){
                            classAvailable.add("Class2");
                        }else {
                            classAvailable.add(ds.getKey().replace("_"," "));
                        }
                    }
                }
                showClassList(id,schoolName);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showClassList(final String schoolID, final String schoolName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Class");
        builder.setItems(className, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (classAvailable.contains(className[which])){
                    Intent intent = new Intent(getActivity(), SchoolSetAllDetails.class);
                    intent.putExtra("schoolID",schoolID);
                    intent.putExtra("schoolName",schoolName);
                    intent.putExtra("class",className[which]);
                    intent.putExtra("classSelectedId",which);
                    startActivity(intent);
                    dialog.dismiss();
                }else {
                    Toast.makeText(getContext(), "Book/Copy set not available for this class.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.show();
    }

    private void showEditSchoolInfoDialog(final String schoolId, final String school_name, String schooladdress, String schoolPhone, String setAvailavle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit School Details");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View editDialog = inflater.inflate(R.layout.edit_school_info_dialog,null);
        final TextInputEditText copyTypeEt = editDialog.findViewById(R.id.school_name);
        final TextInputEditText pageCountEt = editDialog.findViewById(R.id.schoolAddress);
        final TextInputEditText copyCostEt = editDialog.findViewById(R.id.school_phone);
        final CheckBox isCopyAvailable = editDialog.findViewById(R.id.new_set_available);
        copyTypeEt.setText(school_name);
        pageCountEt.setText(schooladdress);
        copyCostEt.setText(schoolPhone);

        if (setAvailavle.equals("true")){
            isCopyAvailable.setChecked(true);
        }else {
            isCopyAvailable.setChecked(false);
        }

        builder.setCancelable(false);
        builder.setView(editDialog);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!TextUtils.isEmpty(copyTypeEt.getText().toString()) && !TextUtils.isEmpty(pageCountEt.getText().toString()) &&
                        !TextUtils.isEmpty(copyCostEt.getText().toString())){
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("SchoolName",copyTypeEt.getText().toString());
                    hm.put("Address",pageCountEt.getText().toString());
                    hm.put("Phone",copyCostEt.getText().toString());
                    if (isCopyAvailable.isChecked()) {
                        hm.put("SetAvailable","true");
                    }
                    else {
                        hm.put("SetAvailable","false");
                    }
                    Constant.schoolListRef.child(schoolId).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "School Info Edited!!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(getActivity(), "Please Fill All Fields...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }
        public void setSchoolName(String name)
        {
            TextView userNameView= mView.findViewById(R.id.school_name);
            userNameView.setText(name);

        }
        public void setPhone(String phone)
        {
            TextView userNumberView= mView.findViewById(R.id.phone_number);
            userNumberView.setText(phone);
        }

        public void setSchoolImage(String url,Context ctx){
            ImageView iv = mView.findViewById(R.id.image_view);
            Glide.with(ctx).load(url).into(iv);
        }
        public void setAlphabetImage(String s){
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(s, randomColor());
            ImageView image = mView.findViewById(R.id.image_view);
            image.setImageDrawable(drawable);
        }
        public int randomColor() {

            int r = (int) (0xff * Math.random());
            int g = (int) (0xff * Math.random());
            int b = (int) (0xff * Math.random());

            return Color.rgb( r, g, b);
        }

    }

}
