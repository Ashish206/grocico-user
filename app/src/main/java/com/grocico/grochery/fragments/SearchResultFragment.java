package com.grocico.grochery.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.ItemsGridAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.ItemsGridModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchResultFragment extends Fragment implements ItemsGridAdapter.AddToCartBtnListener  {
    ApiInterface apiInterface;
    RecyclerView rcv;
    ProgressDialog pd;
    TextView noItemTxt;
    View v;
    public static SearchResultFragment ctx;
    static ItemsGridAdapter itemsGridAdapter;
    public static ArrayList<ItemsGridModel> itemList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_search_result, container, false);

        rcv= v.findViewById(R.id.subCatrecv);
        rcv.setHasFixedSize(true);
        rcv.setLayoutManager(new LinearLayoutManager(getContext()));

        ctx = this;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        pd = new ProgressDialog(getContext());
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);

        noItemTxt = v.findViewById(R.id.noItemTxt);
        itemsGridAdapter = new ItemsGridAdapter(getContext(),itemList,SearchResultFragment.this);
        rcv.setAdapter(itemsGridAdapter);

        rcv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });
        pd.show();
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }

    public void loadData()
    {
        getSearchResult(Constant.searchKey).enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getContext(),response.body(),SearchResultFragment.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//                Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private Call<List<ItemsGridModel>> getSearchResult(String searchKey) {
        return apiInterface.getSearchResult(
                searchKey,
                Constant.searchCityName
        );
    }

    @Override
    public void onClick(final String itemId) {
        pd.show();
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        hm.put("itemId",itemId);
        hm.put("quantity","1");
        hm.put("size","-");

        c.getDataFromServer(getContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                pd.dismiss();
                if (!response.equals("error")){
                    Toast.makeText(getContext(), "Added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                }
            }
        },hm);
    }

}

