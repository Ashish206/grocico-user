package com.grocico.grochery.model;

public class EditAvailableClassModel {
    String isAvailable;

    public EditAvailableClassModel() {
    }

    public EditAvailableClassModel(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getIsAvailable() {
        return isAvailable;
    }
}
