package com.grocico.grochery.model;

public class ItemDetailModel {
    String id, itemId, itemname, category, sub_category, mrp, sale_price, weight, description, author, image, feature_image, size, colors, quantity, rating, soldBy, date, sale_price_list, mrp_list;


    public ItemDetailModel() {
    }

    public ItemDetailModel(String id, String itemId, String itemname, String category, String sub_category, String mrp, String sale_price, String weight, String description, String author, String image, String feature_image, String size, String colors, String quantity, String rating, String soldBy, String date, String sale_price_list, String mrp_list) {
        this.id = id;
        this.itemId = itemId;
        this.itemname = itemname;
        this.category = category;
        this.sub_category = sub_category;
        this.mrp = mrp;
        this.sale_price = sale_price;
        this.weight = weight;
        this.description = description;
        this.author = author;
        this.image = image;
        this.feature_image = feature_image;
        this.size = size;
        this.colors = colors;
        this.quantity = quantity;
        this.rating = rating;
        this.soldBy = soldBy;
        this.date = date;
        this.sale_price_list = sale_price_list;
        this.mrp_list = mrp_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFeature_image() {
        return feature_image;
    }

    public void setFeature_image(String feature_image) {
        this.feature_image = feature_image;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(String soldBy) {
        this.soldBy = soldBy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSale_price_list() {
        return sale_price_list;
    }

    public void setSale_price_list(String sale_price_list) {
        this.sale_price_list = sale_price_list;
    }

    public String getMrp_list() {
        return mrp_list;
    }

    public void setMrp_list(String mrp_list) {
        this.mrp_list = mrp_list;
    }
}