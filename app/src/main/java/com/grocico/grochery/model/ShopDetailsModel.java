package com.grocico.grochery.model;

public class ShopDetailsModel {

    String shopname,shopcode,imageurl,category;

    public ShopDetailsModel() {
    }

    public ShopDetailsModel(String shopname, String shopcode, String imageurl, String category) {
        this.shopname = shopname;
        this.shopcode = shopcode;
        this.imageurl = imageurl;
        this.category = category;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getShopcode() {
        return shopcode;
    }

    public void setShopcode(String shopcode) {
        this.shopcode = shopcode;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
