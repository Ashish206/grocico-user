package com.grocico.grochery.model;

public class subCategoryModel {

    public subCategoryModel(){

    }
    public subCategoryModel(String sub_category_id, String category_id, String subcategory, String subcategory_image, String shop_code) {
        this.sub_category_id = sub_category_id;
        this.category_id = category_id;
        Subcategory = subcategory;
        this.subcategory_image = subcategory_image;
        this.shop_code = shop_code;
    }

    String sub_category_id,category_id,Subcategory,subcategory_image,shop_code;

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory() {
        return Subcategory;
    }

    public void setSubcategory(String subcategory) {
        Subcategory = subcategory;
    }

    public String getSubcategory_image() {
        return subcategory_image;
    }

    public void setSubcategory_image(String subcategory_image) {
        this.subcategory_image = subcategory_image;
    }

    public String getShop_code() {
        return shop_code;
    }

    public void setShop_code(String shop_code) {
        this.shop_code = shop_code;
    }
}
