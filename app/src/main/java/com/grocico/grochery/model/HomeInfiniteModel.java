
package com.grocico.grochery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeInfiniteModel {

    @SerializedName("heading")
    @Expose
    private Heading heading;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Heading getHeading() {
        return heading;
    }

    public void setHeading(Heading heading) {
        this.heading = heading;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

}
