package com.grocico.grochery.model;

public class SearchResultModel {
    String searchResult;

    public SearchResultModel() {
    }

    public SearchResultModel(String searchResult) {
        this.searchResult = searchResult;
    }

    public String getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(String searchResult) {
        this.searchResult = searchResult;
    }
}
