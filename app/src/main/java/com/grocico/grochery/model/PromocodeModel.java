package com.grocico.delivermybook.model;

public class PromocodeModel {
    String code,shopCode;

    public PromocodeModel() {
    }

    public PromocodeModel(String code, String shopCode) {
        this.code = code;
        this.shopCode = shopCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
}
