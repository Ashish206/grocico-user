package com.grocico.grochery.model;

import static com.grocico.grochery.activity.MainActivity.bookSetList;

public class CartItemModel {
    String itemname,itemId,mrp,sale_price,feature_image,quantity,category,sub_category,weight,author,size,colors,soldBy,rating,cartItemId,cartItemQuantity,cartItemSize,sale_price_list,mrp_list,cartItemPricePosition;

    public CartItemModel() {
    }

    public CartItemModel(String itemname, String itemId, String mrp, String sale_price, String feature_image, String quantity, String category, String sub_category, String weight, String author, String size, String colors, String soldBy, String rating, String cartItemId, String cartItemQuantity, String cartItemSize, String sale_price_list, String mrp_list, String cartItemPricePosition) {
        this.itemname = itemname;
        this.itemId = itemId;
        this.mrp = mrp;
        this.sale_price = sale_price;
        this.feature_image = feature_image;
        this.quantity = quantity;
        this.category = category;
        this.sub_category = sub_category;
        this.weight = weight;
        this.author = author;
        this.size = size;
        this.colors = colors;
        this.soldBy = soldBy;
        this.rating = rating;
        this.cartItemId = cartItemId;
        this.cartItemQuantity = cartItemQuantity;
        this.cartItemSize = cartItemSize;
        this.sale_price_list = sale_price_list;
        this.mrp_list = mrp_list;
        this.cartItemPricePosition = cartItemPricePosition;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getFeature_image() {
        return feature_image;
    }

    public void setFeature_image(String feature_image) {
        this.feature_image = feature_image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(String soldBy) {
        this.soldBy = soldBy;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getCartItemQuantity() {
        return cartItemQuantity;
    }

    public void setCartItemQuantity(String cartItemQuantity) {
        this.cartItemQuantity = cartItemQuantity;
    }

    public String getCartItemSize() {
        return cartItemSize;
    }

    public void setCartItemSize(String cartItemSize) {
        this.cartItemSize = cartItemSize;
    }

    public String getSale_price_list() {
        return sale_price_list;
    }

    public void setSale_price_list(String sale_price_list) {
        this.sale_price_list = sale_price_list;
    }

    public String getMrp_list() {
        return mrp_list;
    }

    public void setMrp_list(String mrp_list) {
        this.mrp_list = mrp_list;
    }

    public String getCartItemPricePosition() {
        return cartItemPricePosition;
    }

    public void setCartItemPricePosition(String cartItemPricePosition) {
        this.cartItemPricePosition = cartItemPricePosition;
    }

    public boolean isItemAvailableInCart(String id){
        boolean isExist = false;
        for (int p = 0;p< bookSetList.size();p++){
            if (bookSetList.get(p).getItemId().equals(id)){ isExist = true; }
        }
        return isExist;
    }
}
