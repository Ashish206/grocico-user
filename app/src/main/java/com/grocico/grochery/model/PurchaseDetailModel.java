package com.grocico.grochery.model;

public class PurchaseDetailModel {

    String order_id,itemname,feature_image,category,sub_category,mrp,sale_price,quantity,weight,author,size,colors,soldBy,itemId,rated;

    public PurchaseDetailModel() {
    }

    public PurchaseDetailModel(String order_id, String itemname, String feature_image, String category, String sub_category, String mrp, String sale_price, String quantity, String weight, String author, String size, String colors, String soldBy, String itemId, String rated) {
        this.order_id = order_id;
        this.itemname = itemname;
        this.feature_image = feature_image;
        this.category = category;
        this.sub_category = sub_category;
        this.mrp = mrp;
        this.sale_price = sale_price;
        this.quantity = quantity;
        this.weight = weight;
        this.author = author;
        this.size = size;
        this.colors = colors;
        this.soldBy = soldBy;
        this.itemId = itemId;
        this.rated = rated;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getFeature_image() {
        return feature_image;
    }

    public void setFeature_image(String feature_image) {
        this.feature_image = feature_image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(String soldBy) {
        this.soldBy = soldBy;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }
}
