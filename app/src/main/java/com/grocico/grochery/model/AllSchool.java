package com.grocico.grochery.model;

public class AllSchool {
    String SchoolName,Phone,Address,SetAvailable;

    public AllSchool(){

    }

    public AllSchool(String schoolName, String phone, String address, String setAvailable) {
        SchoolName = schoolName;
        Phone = phone;
        Address = address;
        SetAvailable = setAvailable;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getSetAvailable() {
        return SetAvailable;
    }

    public void setSetAvailable(String setAvailable) {
        SetAvailable = setAvailable;
    }
}
