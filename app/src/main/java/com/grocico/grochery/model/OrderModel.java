package com.grocico.grochery.model;

public class OrderModel {

    String order_id,date,time,deliveryStatus,userId,userName,userPhone,userState,userCity,userAddress,userLandmark,userPostalCode,userLat,userTokenId,filterDate,userLng,paymentMethod,referralAmount,deliveryCharge,TotalAmount,ActualTotalAmount,orderTitle,deliveryBoyName,deliveryBoyNumber,deliveryBoyId,interest;

    public OrderModel() {
    }

    public OrderModel(String order_id, String date, String time, String deliveryStatus, String userId, String userName, String userPhone, String userState, String userCity, String userAddress, String userLandmark, String userPostalCode, String userLat, String userTokenId, String filterDate, String userLng, String paymentMethod, String referralAmount, String deliveryCharge, String totalAmount, String actualTotalAmount, String orderTitle, String deliveryBoyName, String deliveryBoyNumber, String deliveryBoyId, String interest) {
        this.order_id = order_id;
        this.date = date;
        this.time = time;
        this.deliveryStatus = deliveryStatus;
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userState = userState;
        this.userCity = userCity;
        this.userAddress = userAddress;
        this.userLandmark = userLandmark;
        this.userPostalCode = userPostalCode;
        this.userLat = userLat;
        this.userTokenId = userTokenId;
        this.filterDate = filterDate;
        this.userLng = userLng;
        this.paymentMethod = paymentMethod;
        this.referralAmount = referralAmount;
        this.deliveryCharge = deliveryCharge;
        TotalAmount = totalAmount;
        ActualTotalAmount = actualTotalAmount;
        this.orderTitle = orderTitle;
        this.deliveryBoyName = deliveryBoyName;
        this.deliveryBoyNumber = deliveryBoyNumber;
        this.deliveryBoyId = deliveryBoyId;
        this.interest = interest;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserLandmark() {
        return userLandmark;
    }

    public void setUserLandmark(String userLandmark) {
        this.userLandmark = userLandmark;
    }

    public String getUserPostalCode() {
        return userPostalCode;
    }

    public void setUserPostalCode(String userPostalCode) {
        this.userPostalCode = userPostalCode;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserTokenId() {
        return userTokenId;
    }

    public void setUserTokenId(String userTokenId) {
        this.userTokenId = userTokenId;
    }

    public String getFilterDate() {
        return filterDate;
    }

    public void setFilterDate(String filterDate) {
        this.filterDate = filterDate;
    }

    public String getUserLng() {
        return userLng;
    }

    public void setUserLng(String userLng) {
        this.userLng = userLng;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getReferralAmount() {
        return referralAmount;
    }

    public void setReferralAmount(String referralAmount) {
        this.referralAmount = referralAmount;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getActualTotalAmount() {
        return ActualTotalAmount;
    }

    public void setActualTotalAmount(String actualTotalAmount) {
        ActualTotalAmount = actualTotalAmount;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public void setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
    }

    public String getDeliveryBoyNumber() {
        return deliveryBoyNumber;
    }

    public void setDeliveryBoyNumber(String deliveryBoyNumber) {
        this.deliveryBoyNumber = deliveryBoyNumber;
    }

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}
