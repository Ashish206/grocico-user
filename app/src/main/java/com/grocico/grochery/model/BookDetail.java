package com.grocico.grochery.model;

public class BookDetail {
    String bookName;
    String bookCost;
    String publisher;
    String isBookAvailable;

    public BookDetail(){

    }

    public BookDetail(String bookName, String bookCost, String publisher, String isBookAvailable) {
        this.bookName = bookName;
        this.bookCost = bookCost;
        this.publisher = publisher;
        this.isBookAvailable = isBookAvailable;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookCost() {
        return bookCost;
    }

    public void setBookCost(String bookCost) {
        this.bookCost = bookCost;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsBookAvailable() {
        return isBookAvailable;
    }

    public void setIsBookAvailable(String isBookAvailable) {
        this.isBookAvailable = isBookAvailable;
    }
}
