package com.grocico.grochery.model;

public class ItemsGridModel {

    String itemname,itemId,mrp,sale_price,feature_image,quantity,size,author;

    public ItemsGridModel(String itemname, String itemId, String mrp, String sale_price, String feature_image, String quantity, String size, String author) {
        this.itemname = itemname;
        this.itemId = itemId;
        this.mrp = mrp;
        this.sale_price = sale_price;
        this.feature_image = feature_image;
        this.quantity = quantity;
        this.size = size;
        this.author = author;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getFeature_image() {
        return feature_image;
    }

    public void setFeature_image(String feature_image) {
        this.feature_image = feature_image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
