package com.grocico.grochery.model;

public class CopyDetails {
    String copyCost,copyType,pagesCount,isCopyAvailable;

    public CopyDetails(){}

    public CopyDetails(String copyCost, String copyType, String pagesCount, String isCopyAvailable) {
        this.copyCost = copyCost;
        this.copyType = copyType;
        this.pagesCount = pagesCount;
        this.isCopyAvailable = isCopyAvailable;
    }

    public String getCopyCost() {
        return copyCost;
    }

    public void setCopyCost(String copyCost) {
        this.copyCost = copyCost;
    }

    public String getCopyType() {
        return copyType;
    }

    public void setCopyType(String copyType) {
        this.copyType = copyType;
    }

    public String getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(String pagesCount) {
        this.pagesCount = pagesCount;
    }

    public String getIsCopyAvailable() {
        return isCopyAvailable;
    }

    public void setIsCopyAvailable(String isCopyAvailable) {
        this.isCopyAvailable = isCopyAvailable;
    }
}
