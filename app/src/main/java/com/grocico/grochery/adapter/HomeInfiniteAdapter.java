package com.grocico.grochery.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.activity.ItemListActivity;
import com.grocico.grochery.activity.MainActivity;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.Data;
import com.grocico.grochery.model.HomeInfiniteModel;
import com.grocico.grochery.model.ItemsGridModel;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeInfiniteAdapter  extends  RecyclerView.Adapter<HomeInfiniteAdapter.ItemViewHolder> implements HomeInfiniteMenuAdapter.OnClick,BestSellerAdapter.AddToCartBtnListener{

    OnBannerClick onClick;
    List<HomeInfiniteModel> list;
    Context ctx;
    ApiInterface apiInterface;
    public HomeInfiniteAdapter(List<HomeInfiniteModel> list,Context ctx,OnBannerClick onClick,ApiInterface apiInterface) {
        this.list = list;
        this.ctx = ctx;
        this.onClick = onClick;
        this.apiInterface = apiInterface;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v1 = inflater.inflate(R.layout.single_home_infinite_main, parent, false);
        return new ItemViewHolder(v1);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
       holder.setHeading(list.get(position).getHeading().getCategoryName());
       holder.setBottomImage(list.get(position).getHeading().getCategoryImage(),ctx);
       holder.setMiddleList(list.get(position).getData(),ctx,list.get(position).getHeading().getCategoryName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(String key,String category) {
        Intent i = new Intent(ctx, ItemListActivity.class);
        i.putExtra("subcategory",key);
        i.putExtra("category",category);
        ctx.startActivity(i);
    }

    @Override
    public void onClick(String itemId) {
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        hm.put("itemId",itemId);
        hm.put("quantity","1");
        hm.put("size","-");

        c.getDataFromServer(ctx, Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                if (!response.equals("error")){
                    Toast.makeText(ctx, "Added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(ctx, "error", Toast.LENGTH_SHORT).show();
                }

            }
        },hm);
    }

    public interface OnBannerClick{
        void onBannerClick(String category);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        View v;
        String category;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            v =itemView;
            v.setOnClickListener(this);
        }

        public void setHeading(String heading){
            TextView tv = v.findViewById(R.id.category_name);
            tv.setText(Constant.upperCaseWords(heading));
            category = heading;
        }

        public void setMiddleList(List<Data> data, final Context ctx, String category){
            final RecyclerView tv = v.findViewById(R.id.list);
            tv.setHasFixedSize(true);
            tv.setLayoutManager(new LinearLayoutManager(ctx,LinearLayoutManager.HORIZONTAL,false));

            Call<List<ItemsGridModel>> call2 = apiInterface.getHomeItemsByCategory(category,Constant.searchCityName);
            call2.enqueue(new Callback<List<ItemsGridModel>>() {
                @Override
                public void onResponse(Call<List<ItemsGridModel>> call, Response<List<ItemsGridModel>> response) {
                    Log.d("---------->",response.toString());
                    BestSellerAdapter itemsGridAdapter = new BestSellerAdapter(ctx,response.body(), HomeInfiniteAdapter.this);
                    tv.setAdapter(itemsGridAdapter);
                }

                @Override
                public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                    Log.d("---------->f",t.getMessage().toString());
                }
            });
        }

        public void setBottomImage(String heading, Context ctx){
            ImageView tv = v.findViewById(R.id.image);
            Glide.with(ctx).load(heading).into(tv);
        }

        @Override
        public void onClick(View v) {
            if (onClick!=null){
                onClick.onBannerClick(category);
            }
        }
    }
}