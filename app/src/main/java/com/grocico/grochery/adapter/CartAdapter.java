package com.grocico.grochery.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.activity.CartActivity;
import com.grocico.grochery.activity.ChangeAddressActivity;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.CartItemModel;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;

import static com.grocico.grochery.activity.CartActivity.proceedBtn;
import static com.grocico.grochery.activity.CartActivity.progressDialog;

public class CartAdapter extends HFRecyclerView<CartItemModel> {
    protected ItemListener mListener;
    private Context context;
    private ArrayList<CartItemModel> bookDataList;
    CartItemModel temp;
    RemoveCartItem removeCartItem;
    EditCartItem editCartItem;


    public CartAdapter(Context context, ArrayList values, ItemListener itemListener){
        super(true, true);
        this.context = context;
        this.bookDataList = values;
        this.mListener = itemListener;
        this.removeCartItem = (RemoveCartItem) itemListener;
        this.editCartItem = (EditCartItem) itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            temp = bookDataList.get(i-1);
            holder.setItemName(Constant.upperCaseWords(temp.getItemname()));
            holder.setItemImage(temp.getFeature_image(),context);
            holder.setItemCost(temp.getSale_price());
            holder.setItemActualCost(temp.getMrp());
            holder.setPublisherName(temp.getCategory());
            holder.setAuthorName(temp.getAuthor());
            holder.setPersentageText(Float.parseFloat(temp.getSale_price()),Float.parseFloat(temp.getMrp()));
            holder.setKey(temp.getItemId());
            holder.setQuantity(temp.getCartItemQuantity());
            holder.setSize(temp.getCartItemSize(),temp.getWeight());
            holder.setRemoveBtn(temp.getItemId(),temp.getCategory());
//            holder.setEditBtn(temp.getItemId(),temp.getCategory(),temp.getQuantity());
            holder.setEditBtn(temp.getItemId(),temp.getCategory(),String.valueOf(Constant.maxOrderQuantity));
            proceedBtn.setEnabled(true);
            progressDialog.dismiss();


        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.setNamePostalCode(Constant.upperCaseWords(Constant.userName)+", "+Constant.postalCode);
            holder.setAddress(Constant.userAddress);
            holder.changeAddress();
        } else if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder holder = (FooterViewHolder) viewHolder;

            holder.setItemCount(Constant.CartItemCount);

            holder.setTotalPrice(String.valueOf((int) CartActivity.totalAmount));
            holder.setReferralAmount((int)CartActivity.totalAmount);
            holder.setTotalPayable(String.valueOf((int)CartActivity.totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
            holder.setDeliveryCharge(Constant.deliveryCharge);
            holder.setSaveTxt(String.valueOf((int)CartActivity.totalActualAmount-CartActivity.totalAmount+Constant.useableReferralAmount));
        }

    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.single_cart_item_list, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.cart_header_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.cart_footer_layout, parent, false));
    }


    public interface ItemListener {
        void onItemClick(String key, ImageView imageView);
    }


    public interface RemoveCartItem{
        void onRemoveItemClick(String key,String category);
    }

    public interface EditCartItem{
        void onEditCartItemListener(String key,String category,String quantity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String key;
        View mView;
        ImageView itemImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setSize(String item,String weight) {
            TextView item_name = mView.findViewById(R.id.special_price);
            if (!item.equals("-")){
                item_name.setText(item);
            }else {
                item_name.setText(weight);
            }
        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(String item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setRemoveBtn(final String item, final String cat){
            TextView remmoveBtn = mView.findViewById(R.id.delete_cart);

            remmoveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Constant.cartRef.child(item).setValue(null);

                    if (removeCartItem != null) {
                        removeCartItem.onRemoveItemClick(item,cat);
                    }

                }
            });
        }
        public void setEditBtn(final String item, final String cat, final String quantity){
            TextView edit = mView.findViewById(R.id.edit_cart);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Constant.cartRef.child(item).setValue(null);

                    if (editCartItem != null) {
                        editCartItem.onEditCartItemListener(item,cat,quantity);
                    }

                }
            });
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key,itemImage);
            }
        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        View mView;
        HeaderViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setNamePostalCode(String item) {
            TextView item_name = mView.findViewById(R.id.name_postal_code);
            item_name.setText(item);
        }

        public void setAddress(String item) {
            TextView item_name = mView.findViewById(R.id.address);
            item_name.setText(item);
        }

        public void changeAddress() {
            TextView item_name = mView.findViewById(R.id.change_adr);
            item_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChangeAddressActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        View mView;
        FooterViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemCount(String item) {
            TextView item_name = mView.findViewById(R.id.total_item);
            item_name.setText("Price ("+item+" item)");
        }

        public void setTotalPrice(String item) {
            TextView item_name = mView.findViewById(R.id.total_price);
            item_name.setText("₹"+item);
        }

        public void setTotalPayable(String item) {
            TextView item_name = mView.findViewById(R.id.total_amount_payable);
            item_name.setText("₹"+item);
        }



        public void setDeliveryCharge(String item) {
            TextView item_name = mView.findViewById(R.id.delivery_charge);
            item_name.setText("₹"+item);
        }

        public void setReferralAmount(int item) {
            LinearLayout ll = mView.findViewById(R.id.referral_lay);
            TextView item_name = mView.findViewById(R.id.referral_amount);
            if (item!=0 && item>=250 && Constant.referralAmount>=10){
                ll.setVisibility(View.VISIBLE);
                item_name.setText("- ₹"+((Constant.referralAmount*20)/100));
                item_name.setTextColor(context.getResources().getColor(R.color.red));
                Constant.useableReferralAmount = ((Constant.referralAmount*20)/100);
            }else {
                Constant.useableReferralAmount=0;
                ll.setVisibility(View.GONE);
                item_name.setText("₹"+0);
            }

        }

        public void setSaveTxt(String item) {
            TextView item_name = mView.findViewById(R.id.save_txt);
            item_name.setText("You will save total ₹"+item+" on this order");
        }

    }
}

