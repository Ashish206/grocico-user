package com.grocico.grochery.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.activity.ItemDetailActivity;
import com.grocico.grochery.model.ItemsGridModel;

import java.util.List;

public class BestSellerAdapter extends RecyclerView.Adapter<BestSellerAdapter.itemViewHolder> {

    ItemsGridModel tmp;
    Context ctx;
    List<ItemsGridModel> arrayList;
    AddToCartBtnListener addToCartBtnListener;
    public BestSellerAdapter(Context ctx, List<ItemsGridModel> arrayList, AddToCartBtnListener listener) {
        this.ctx = ctx;
        this.arrayList = arrayList;
        addToCartBtnListener = listener;
    }

    @NonNull
    @Override
    public BestSellerAdapter.itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.single_best_seller_layout, parent, false);
        return new BestSellerAdapter.itemViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull BestSellerAdapter.itemViewHolder holder, int position) {
        tmp=arrayList.get(position);
        holder.setfeatureImage(tmp.getFeature_image(),ctx);
        holder.setItemName(tmp.getItemname());
        holder.setMrp(tmp.getMrp());
        holder.setSalePrice(tmp.getSale_price());
        holder.setPersentageText(Float.parseFloat(tmp.getSale_price()),Float.parseFloat(tmp.getMrp()));
        holder.setItemId(tmp.getItemId(),tmp.getSize());
        holder.setAddToCart(tmp.getQuantity());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public interface AddToCartBtnListener {
        void onClick(String itemId);
    }


    public class itemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View v;
        Button addToCartBtn;
        String itemId;
        String size;

        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            v=itemView;
            addToCartBtn = v.findViewById(R.id.add_to_cart);
            addToCartBtn.setOnClickListener(this);
            v.findViewById(R.id.cardView).setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.add_to_cart){
                if (addToCartBtnListener!=null){
                    if (size.equals("-")){
                        addToCartBtnListener.onClick(itemId);
                    }else {
                        Toast.makeText(ctx, "Select size to add in cart", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ctx, ItemDetailActivity.class);
                        i.putExtra("itemId",itemId);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(i);
                    }

                }
            }else {
                Intent i = new Intent(ctx, ItemDetailActivity.class);
                i.putExtra("itemId",itemId);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(i);
            }
        }

        public void setItemId(String key,String size){
            itemId = key;
            this.size = size;
        }

        public void setAddToCart(String quantity){
            if (Integer.valueOf(quantity)<=0){
                addToCartBtn.setClickable(false);
                addToCartBtn.setBackground(ctx.getDrawable(R.drawable.round_corner_button_gray));
                addToCartBtn.setTextColor(ctx.getResources().getColor(R.color.txt_dark_gray));
                addToCartBtn.setText("Unavailable");
            }else {
                addToCartBtn.setClickable(true);
                addToCartBtn.setBackground(ctx.getDrawable(R.drawable.round_corner_button));
                addToCartBtn.setText("Add");
            }

        }

        public void setfeatureImage(String url, Context ctx){
            ImageView iv = v.findViewById(R.id.product_image);
            Glide.with(ctx).load(url).into(iv);
        }

        public void setItemName(String itemName)
        {
            TextView tv = v.findViewById(R.id.tvItemName);
            tv.setText(itemName);
        }
        public  void setSalePrice(String salePrice)
        {
            TextView tv = v.findViewById(R.id.tvSalePrice);
            tv.setText("₹"+salePrice);
        }
        public  void setMrp(String mrp)
        {
            TextView tv = v.findViewById(R.id.tvMRP);
            tv.setText("₹"+mrp);
        }
        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = v.findViewById(R.id.percentageTxt);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage+"% off");
        }
    }
}
