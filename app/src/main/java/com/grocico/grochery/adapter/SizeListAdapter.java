package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.grocico.grochery.R;

public class SizeListAdapter extends RecyclerView.Adapter<SizeListAdapter.ViewHolder>{
    String[] sizeList;
    ItemListener itemListener;
    Context ctx;
    int posn;

    public SizeListAdapter(String[] sizelist,ItemListener itemListener,Context ctx,int position) {
        this.sizeList = sizelist;
        this.itemListener = itemListener;
        this.ctx = ctx;
        this.posn = position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.size_list_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setSize(sizeList[position]);
        holder.setPosition(position);
        if (position ==posn){
            holder.setSelected();
        }else {
            holder.setUnSelected();
        }

    }

    @Override
    public int getItemCount() {
        return sizeList.length;
    }

    public interface ItemListener {
        void onItemClick(String size,int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        TextView sizeTxt;
        String size;
        int position;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sizeTxt = itemView.findViewById(R.id.size_text);
            sizeTxt.setOnClickListener(this);
        }

        public void setSize(String size){
            this.size = size;
            sizeTxt.setText(size);
        }

        public void setPosition(int position){
            this.position = position;
        }

        public void setSelected(){
            sizeTxt.setBackground(ctx.getDrawable(R.drawable.rectangle_auto));
        }
        public void setUnSelected(){
            sizeTxt.setBackground(ctx.getDrawable(R.drawable.rectangle_gray));
        }

        @Override
        public void onClick(View view) {
            if (itemListener!=null){
                itemListener.onItemClick(size,position);
            }
        }
    }
}
