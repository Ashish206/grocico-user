package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;

import java.util.ArrayList;


public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private Integer[] images = {};
    ArrayList imageUrlList;
    ArrayList clickArrayList;
    String query = "";
    int x = 1;
    String headingTxt;

    public ViewPagerAdapter(Context context, ArrayList imageUrlList) {
        this.context = context;
        this.imageUrlList = imageUrlList;

    }

    @Override
    public int getCount() {
        return imageUrlList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        ImageView imageView = view.findViewById(R.id.imageView);
        Glide.with(context).load(imageUrlList.get(position)).into(imageView);

//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(position == 0){
//                    if (!clickArrayList.get(0).toString().equals("null")){
//                        setRecyclerView(clickArrayList.get(0).toString());
//                    }
//                } else if(position == 1){
//                    if (!clickArrayList.get(1).toString().equals("null")){
//                        setRecyclerView(clickArrayList.get(1).toString());
//                    }
//                } else {
//                    if (!clickArrayList.get(2).toString().equals("null")){
//                        setRecyclerView(clickArrayList.get(2).toString());
//                    }
//                }
//
//            }
//        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
