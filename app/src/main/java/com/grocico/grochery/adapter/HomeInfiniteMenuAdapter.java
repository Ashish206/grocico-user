package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.Data;

import java.util.List;

public class HomeInfiniteMenuAdapter  extends  RecyclerView.Adapter<HomeInfiniteMenuAdapter.ItemViewHolder>  {

    OnClick onClick;
    List<Data> list;
    Context ctx;
    String category;

    public HomeInfiniteMenuAdapter(List<Data> list,Context ctx,OnClick onClick,String category) {
        this.list = list;
        this.ctx = ctx;
        this.onClick =  onClick;
        this.category = category;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v1 = inflater.inflate(R.layout.single_home_infinite_menu_layout, parent, false);
        return new ItemViewHolder(v1);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.setData(list.get(position).getSubcategory(),list.get(position).getSubcategoryImage(),ctx);
    }


    public interface OnClick{
        void onClick(String key,String category);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        View v;
        String key;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            v =itemView;
            v.setOnClickListener(this);
        }

        public void setData(String name,String url,Context ctx){
            TextView tv = v.findViewById(R.id.textView);
            tv.setText(Constant.upperCaseWords(name));
            key = name;
            ImageView iv = v.findViewById(R.id.imageView);
            Glide.with(ctx).load(url).into(iv);
        }

        @Override
        public void onClick(View v) {
            if (onClick != null){
                onClick.onClick(key,category);
            }
        }
    }
}
