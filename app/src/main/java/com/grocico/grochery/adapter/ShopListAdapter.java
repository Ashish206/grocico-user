package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.ShopDetailsModel;

import java.util.List;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.itemViewHolder> {
    ShopDetailsModel tmp;
    Context ctx;
    protected ItemListener mListener;
    List<ShopDetailsModel> arrayList;

    public ShopListAdapter(Context ctx, List<ShopDetailsModel> arrayList,ItemListener itemListener){
        this.ctx = ctx;
        this.arrayList = arrayList;
        mListener=itemListener;
    }

    @NonNull
    @Override
    public itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.single_shop_details_grid_layout, parent, false);
        return new itemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull itemViewHolder holder, int position) {
        tmp=arrayList.get(position);
        holder.setShopCode(tmp.getShopcode(),tmp.getShopname());
        holder.setImage(tmp.getImageurl(),ctx);
        holder.setName(Constant.upperCaseWords(tmp.getShopname()));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public interface ItemListener {
        void onItemClick(String subCat,String shopName);
    }

    public class itemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View v;
        String subCat,shopName;

        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            v=itemView;
            v.findViewById(R.id.shop_image).setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(subCat,shopName);
            }
        }

        public void setShopCode(String subCat,String shopName)
        {
            this.subCat=subCat;
            this.shopName = shopName;
        }

        public void setImage(String url, Context ctx){
            ImageView iv = v.findViewById(R.id.shop_image);
            Glide.with(ctx).load(url).into(iv);
        }
        public void setName(String url){
            TextView iv = v.findViewById(R.id.shop_name);
            iv.setText(url);
        }

    }
}
