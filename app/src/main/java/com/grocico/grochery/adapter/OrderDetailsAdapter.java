package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.OrderModel;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.HistoryViewHolder>  {

    OnClick onClick;
    Context context;
    ArrayList<OrderModel> orderList;
    public OrderDetailsAdapter(Context ctx, ArrayList<OrderModel> list, OnClick onClick){
        context = ctx;
        orderList = list;
        this.onClick = onClick;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_history_layout, parent, false);

        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {

        OrderModel model = orderList.get(position);

        holder.setOrderDate(model.getDate());
        holder.setBookName(Constant.upperCaseWords(model.getOrderTitle()));
        holder.setCost(model.getTotalAmount());
        holder.setActualCost(model.getActualTotalAmount());
        holder.setDiscount(Float.parseFloat(model.getTotalAmount())-Float.parseFloat(model.getDeliveryCharge()),Float.parseFloat(model.getActualTotalAmount()));
        holder.setDeliveryStatus(model.getDeliveryStatus(),context);
        holder.setDeliveryBoy(model.getDeliveryBoyName());
        holder.setOrderKey(model);
        holder.mView.findViewById(R.id.deliveryBtn).setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public interface OnClick{
        public void onClick(OrderModel order_id);
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        View mView;
        OrderModel orderDetail;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            mView.setOnClickListener(this);
        }

        public void setBookName(final String item) {
            final String[] name ={""};
            final TextView item_name = mView.findViewById(R.id.book_name);
            item_name.setText(item);
        }

        public void setOrderDate(String item) {
            TextView subtxt = mView.findViewById(R.id.order_date);
            subtxt.setText("Ordered On "+item);
        }

        public void setOrderKey(OrderModel key){
            orderDetail = key;
        }
        public void setCost(String item) {
            TextView subtxt = mView.findViewById(R.id.cost_text);
            subtxt.setText("₹"+item);
        }

        public void setActualCost(String item) {
            TextView subtxt = mView.findViewById(R.id.actual_cost_text);
            subtxt.setText("₹"+item);
        }

        public void setDiscount(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setDeliveryBoy(String item) {
            TextView subtxt = mView.findViewById(R.id.deliveryBoyDetails);
            subtxt.setText("Accepted By : "+item);
        }

        public void setDeliveryStatus(String totalCost, Context ctx) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(ctx.getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(ctx.getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(ctx.getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

        @Override
        public void onClick(View view) {
            if (onClick!=null){
                onClick.onClick(orderDetail);
            }
        }
    }
}
