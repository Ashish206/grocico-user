package com.grocico.grochery.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.activity.PurchaseDetailActivity;
import com.grocico.grochery.model.PurchaseDetailModel;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class PurchaseDetailAdapter extends HFRecyclerView<PurchaseDetailModel> {
    private Context context;
    private ArrayList<PurchaseDetailModel> bookDataList;
    PurchaseDetailModel temp;
    CancelOrder cancelOrder;
    Rating ratingListener;

    public PurchaseDetailAdapter(Context context, ArrayList values, CancelOrder cancelOrder,Rating rating){
        super(true, true);
        this.context = context;
        this.bookDataList = values;
        this.cancelOrder = cancelOrder;
        this.ratingListener = rating;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;

            temp = bookDataList.get(i-1);
            Log.d("--------->",temp.getItemname());
            holder.setItemName(temp.getItemname());
            holder.setItemImage(temp.getFeature_image(),context);
            holder.setItemCost(temp.getSale_price());
            holder.setItemActualCost(temp.getMrp());
            holder.setPublisherName(temp.getAuthor());
            holder.setAuthorName(temp.getAuthor());
            holder.setPersentageText(Float.parseFloat(temp.getSale_price()),Float.parseFloat(temp.getMrp()));
            holder.setKey(temp.getItemId());
            holder.setQuantity(temp.getQuantity());
            holder.setSize(temp.getSize(),temp.getWeight());
            holder.ratingBar(temp.getRated(),temp.getItemId(),temp.getOrder_id());

        } else if (viewHolder instanceof HeaderViewHolder) {

            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.setNamePostalCode(PurchaseDetailActivity.userName +", "+PurchaseDetailActivity.userPostalCode);
            holder.setAddress(PurchaseDetailActivity.userAddress);
            holder.setDeliveryStatus(PurchaseDetailActivity.delivered);

        } else if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder holder = (FooterViewHolder) viewHolder;
            holder.setItemCount(String.valueOf(PurchaseDetailActivity.count));
            holder.setTotalPrice(String.valueOf(Integer.valueOf(PurchaseDetailActivity.Cost) - Integer.valueOf(PurchaseDetailActivity.deliveryCharge) - Integer.valueOf(PurchaseDetailActivity.interest)));
            holder.setTotalPayable(String.valueOf(PurchaseDetailActivity.Cost));
            holder.setDeliveryCharge("₹"+PurchaseDetailActivity.deliveryCharge);
            holder.setSaveTxt(String.valueOf(Integer.valueOf(PurchaseDetailActivity.ActualCost)-Integer.parseInt(PurchaseDetailActivity.Cost) + Integer.valueOf(PurchaseDetailActivity.deliveryCharge)));
            holder.setReferralAmount(Integer.valueOf(PurchaseDetailActivity.referralAmount));
            holder.cancelBtn(PurchaseDetailActivity.delivered,PurchaseDetailActivity.orderKey);
            if (PurchaseDetailActivity.paymentMethod.equals("EMI")){
                holder.setInterest("₹"+PurchaseDetailActivity.interest);
            }else {
                holder.mView.findViewById(R.id.interest_lay).setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.history_detail_view_item_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.history_detail_view_header, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.history_detail_view_footer, parent, false));
    }


    public interface CancelOrder{
        void onClacelOrderClick(String key);
    }

    public interface Rating{
        public void onRateingListener(String itemId, float rate, String order_id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        String key;
        View mView;
        ImageView itemImage;
        int x = 0;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setSize(String size,String weight) {
            TextView itemImage = mView.findViewById(R.id.special_price);
            if (size.equals("-")){
                itemImage.setText(weight);
            }else {
                itemImage.setText(size);
            }
        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(String item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }


        public void ratingBar(String rated, final String itemId, final String order_id){
            final MaterialRatingBar materialRatingBar = mView.findViewById(R.id.rating_bar);
            final View v = mView.findViewById(R.id.viw);
            Log.d("--->",rated+"/"+PurchaseDetailActivity.delivered);
            if (rated.equals("false") && PurchaseDetailActivity.delivered.equals("Delivered")){
                materialRatingBar.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                materialRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
                    @Override
                    public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                        ratingListener.onRateingListener(itemId,rating,order_id);
                        materialRatingBar.setVisibility(View.GONE);
                        v.setVisibility(View.GONE);
                    }
                });

            }else {
                materialRatingBar.setVisibility(View.GONE);
                v.setVisibility(View.GONE);
            }

        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        View mView;
        HeaderViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setNamePostalCode(String item) {
            TextView item_name = mView.findViewById(R.id.name_postal_code);
            item_name.setText(item);
        }

        public void setAddress(String item) {
            TextView item_name = mView.findViewById(R.id.address);
            item_name.setText(item);
            item_name.setMaxLines(3);
        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(context.getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        View mView;
        FooterViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemCount(String item) {
            TextView item_name = mView.findViewById(R.id.total_item);
            item_name.setText("Price ("+item+" item)");
        }

        public void setTotalPrice(String item) {
            TextView item_name = mView.findViewById(R.id.total_price);
            item_name.setText("₹"+item);
        }

        public void setTotalPayable(String item) {
            TextView item_name = mView.findViewById(R.id.total_amount_payable);
            item_name.setText("₹"+item);

        }

        public void setDeliveryCharge(String item) {
            TextView item_name = mView.findViewById(R.id.delivery_charge);
            item_name.setText(item);
        }

        public void setInterest(String item) {
            TextView item_name = mView.findViewById(R.id.interest_amount);
            item_name.setText(item);
        }

        public void setSaveTxt(String item) {
            TextView item_name = mView.findViewById(R.id.save_txt);
            item_name.setText("You saved total ₹"+item+" on this order");
        }
        public void setReferralAmount(int item) {
            LinearLayout ll = mView.findViewById(R.id.referral_lay);
            TextView item_name = mView.findViewById(R.id.referral_amount);
            if (item!=0){
                ll.setVisibility(View.VISIBLE);
                item_name.setText("- ₹"+item);
                item_name.setTextColor(context.getResources().getColor(R.color.red));
            }else {
                ll.setVisibility(View.GONE);
                item_name.setText("₹"+0);
            }

        }

        public void cancelBtn(String status, final String key){
            Button cancelBtn = mView.findViewById(R.id.cancel_btn);
            if (status.equals("Order Placed")){
                cancelBtn.setVisibility(View.VISIBLE);
            }else {
                cancelBtn.setVisibility(View.GONE);
            }

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cancelOrder != null) {
                        cancelOrder.onClacelOrderClick(key);
                    }
                }
            });
        }

    }


}

