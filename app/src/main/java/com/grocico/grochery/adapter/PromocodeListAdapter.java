package com.grocico.delivermybook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.grocico.delivermybook.model.PromocodeModel;
import com.grocico.grochery.R;

import java.util.ArrayList;

public class PromocodeListAdapter extends ArrayAdapter<PromocodeModel> {

    public PromocodeListAdapter(Context context, ArrayList<PromocodeModel> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PromocodeModel user = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.single_promo_code_list, parent, false);
        }
        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvHome =convertView.findViewById(R.id.tvHome);
        tvName.setText(user.getShopCode());
        tvHome.setText("Code : "+user.getCode());
        return convertView;
    }

}
