package com.grocico.grochery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.subCategoryModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.itemViewHolder> {

       subCategoryModel tmp;
       Context ctx;
       int position;
      protected ItemListener mListener;
       List<subCategoryModel> arrayList;
    public SubCategoryAdapter(Context ctx, List<subCategoryModel> arrayList,ItemListener itemListener,ArrayList alSubcat) {
        this.ctx = ctx;
        this.arrayList = arrayList;
        mListener=itemListener;
    }

    @NonNull
    @Override
    public itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.single_home_infinite_menu_layout, parent, false);
        return new itemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull itemViewHolder holder, int position) {
       tmp=arrayList.get(position);
       //holder.setImage(tmp.getSubcategory_image(),ctx);
        holder.setData(tmp.getSubcategory(),tmp.getSubcategory_image());
        holder.setSubCat(tmp.getSubcategory());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public interface ItemListener {
        void onItemClick(String subCat);
    }

    public class itemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View v;
        String subCat;
        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            v=itemView;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(subCat);
            }
        }

        public void setData(String name,String url){
            TextView tv = v.findViewById(R.id.textView);
            tv.setText(Constant.upperCaseWords(name));
            ImageView iv = v.findViewById(R.id.imageView);
            Glide.with(ctx).load(url).into(iv);
        }
//
//        public void setImage(String url, Context ctx){
//            ImageView iv = v.findViewById(R.id.imageView);
//            Glide.with(ctx).load(url).into(iv);
//        }

        public void setSubCat(String subCat)
        {
         this.subCat=subCat;
        }

    }
}
