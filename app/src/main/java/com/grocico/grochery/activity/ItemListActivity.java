package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.ItemsGridAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.extra.CountDrawable;
import com.grocico.grochery.fragments.SearchResultFragment;
import com.grocico.grochery.model.ItemsGridModel;
import com.grocico.grochery.model.SearchResultModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.grocico.grochery.activity.MainActivity.bookSetList;

public class ItemListActivity extends AppCompatActivity implements ItemsGridAdapter.AddToCartBtnListener {

    ApiInterface apiInterface;
    RecyclerView rcv;
    ProgressDialog pd;
    String category;
    String subcategory;
    String city=Constant.searchCityName;
    TextView noItemTxt;
    Fragment gridFragment;
    int x=0;
    FragmentManager fragmentManager;
    public static SearchView searchView;
    private SimpleCursorAdapter mAdapter;
    private ApiInterface movieService;
    private ArrayList<SearchResultModel> SUGGESTIONS;
    View gridShowView;
    FrameLayout frameLayout;
    TextView titleText;
    CountDrawable badge;
    ImageView cartBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        category = getIntent().getExtras().getString("category");
        subcategory = getIntent().getExtras().getString("subcategory");
        rcv= findViewById(R.id.subCatrecv);
        rcv.setHasFixedSize(true);
        rcv.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        Log.d("cat from intent---->",""+category);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);
        movieService = ApiClient.getClient().create(ApiInterface.class);
        noItemTxt = findViewById(R.id.noItemTxt);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Constant.upperCaseWords(subcategory));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pd.show();



        searchView = findViewById(R.id.search_bar);
        SUGGESTIONS = new ArrayList();
        gridShowView = findViewById(R.id.grid_show_view);
        frameLayout = findViewById(R.id.frame2);
        frameLayout.setVisibility(View.GONE);
        titleText = findViewById(R.id.titleText);
        fragmentManager = getSupportFragmentManager();

        if (subcategory.equals("null")){
            loadItemByCat();
            titleText.setText(Constant.upperCaseWords(category));
        }
        else if (subcategory.equals("Daily Essentials")){
            loadAllBestSeller();
            titleText.setText(Constant.upperCaseWords(subcategory));
        }
        else if (subcategory.equals("New Arrivals")){
            loadAllNewArrival();
            titleText.setText(Constant.upperCaseWords(subcategory));
        }
        else  {
            loadData();
            titleText.setText(Constant.upperCaseWords(subcategory));
        }



        final String[] from = new String[] {"filter"};
        final int[] to = new int[] {android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.single_search_item,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


        searchView.setSuggestionsAdapter(mAdapter);


        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {

                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {
                Cursor cursor = (Cursor) mAdapter.getItem(i);
                String txt = cursor.getString(cursor.getColumnIndex("filter"));
                searchView.setQuery(txt, true);
                // Toast.makeText(HomeActivity.this, txt, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                // getFinalSearchResult(s);
                Constant.searchKey = s;
                titleText.setText(Constant.upperCaseWords(s));
                if (x==0){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                    gridShowView.setVisibility(View.GONE);
                    searchView.requestFocus();
                    gridFragment = new SearchResultFragment();
                    x=1;
                    fragmentManager.beginTransaction().replace(R.id.frame2,gridFragment)
                            .commit();
                    frameLayout.setVisibility(View.VISIBLE);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try{
                    getSearchResultFromApi(s);

                }catch (Exception e){

                }

                return false;
            }
        });

        cartBtn = findViewById(R.id.cart_btn);
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ItemListActivity.this,CartActivity.class));
            }
        });
        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(this);
        }


        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);


        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton =  this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchView.setQuery(null,false);
                gridShowView.setVisibility(View.VISIBLE);
                searchView.clearFocus();
                titleText.setText(Constant.upperCaseWords(subcategory));
                try {
                    fragmentManager.beginTransaction().remove(gridFragment).commit();
                }catch (Exception e){
                    e.printStackTrace();
                }

                frameLayout.setVisibility(View.GONE);
                x=0;

            }
        });


    }

    private void getSearchResultFromApi(String key) {
        SUGGESTIONS.clear();
        getSearch(key).enqueue(new Callback<List<SearchResultModel>>() {
            @Override
            public void onResponse(Call<List<SearchResultModel>> call, Response<List<SearchResultModel>> response) {
                for(SearchResultModel s : response.body()){
                    SUGGESTIONS.add(s);
                }
                Collections.reverse(SUGGESTIONS);
                populateAdapter();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SearchResultModel>> call, Throwable t) {
                t.printStackTrace();
                SUGGESTIONS.clear();
                populateAdapter();
                mAdapter.notifyDataSetChanged();

            }
        });
    }

    private void populateAdapter() {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "filter" });
        for (int i=0; i<SUGGESTIONS.size(); i++) {
            c.addRow(new Object[] {i, SUGGESTIONS.get(i).getSearchResult()});
        }
        mAdapter.changeCursor(c);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        new Constant().getDataFromServer(getApplicationContext(), Request.Method.POST, "getCartItemCount", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Constant.CartItemCount = String.valueOf(Integer.valueOf(jsonArray.getJSONObject(0).getString("count"))+bookSetList.size());
                    badge.setCount(Constant.CartItemCount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },hm);

    }

    private Call<List<SearchResultModel>> getSearch(String searchKey) {
        return movieService.getSearch(
                searchKey
        );
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        if (x>0){
            gridShowView.setVisibility(View.VISIBLE);
            searchView.clearFocus();
            fragmentManager.beginTransaction().remove(gridFragment).commit();
            frameLayout.setVisibility(View.GONE);
            x=0;
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void loadData()
    {
        getSubcategory(category,subcategory,city).enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getApplicationContext(),response.body(),ItemListActivity.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//              Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    public void loadItemByCat()
    {
        getItemByCat(category,city).enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getApplicationContext(),response.body(),ItemListActivity.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//                Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }


        });
    }

    private void loadAllBestSeller(){
        Log.d("-------->best","enter");
        getAllBestSeller().enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                Log.d("-------->best",response.toString());
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getApplicationContext(),response.body(),ItemListActivity.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }
            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//                Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void loadAllNewArrival() {
        getAllNewArrival().enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getApplicationContext(),response.body(),ItemListActivity.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//                Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }


        });
    }

    private Call<List<ItemsGridModel>> getSubcategory(String category,String subcategory,String city) {
        return apiInterface.getItems(
                category,
                subcategory,
                Constant.searchCityName
        );
    }

    private Call<List<ItemsGridModel>> getItemByCat(String category,String city) {
        return apiInterface.getItemsByCat(
                category,
                Constant.searchCityName
        );
    }

    private Call<List<ItemsGridModel>> getAllBestSeller() {
        return apiInterface.getAllBestSeller(
                Constant.searchCityName
        );
    }

    private Call<List<ItemsGridModel>> getAllNewArrival() {
        return apiInterface.getAllNewArrival(
                Constant.searchCityName
        );
    }

    @Override
    public void onClick(final String itemId) {
        pd.show();
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        hm.put("itemId",itemId);
        hm.put("quantity","1");
        hm.put("size","-");

        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                pd.dismiss();
                if (!response.equals("error")){
                    onResume();
                    Toast.makeText(ItemListActivity.this, "Added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(ItemListActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

            }
        },hm);
    }
}
