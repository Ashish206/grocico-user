package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abdulhakeem.seemoretextview.SeeMoreTextView;
import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.SizeListAdapter;
import com.grocico.grochery.adapter.SliderAdapterExample;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.extra.CountDrawable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.grocico.grochery.activity.MainActivity.bookSetList;

public class ItemDetailActivity extends AppCompatActivity implements SizeListAdapter.ItemListener {

    String rating;
    TextView bookNameTxt,AuthNAmeTxt,costTxt,actualCost,dicsountPercentage,ratingTxt,deliveryTxt,publisherName;
    ImageView imageView;
    SeeMoreTextView disTxt;
    FloatingActionButton back_btn;
    Button addToCart,buyNow,btNotify;
    ImageView cartBtn;
    static CountDrawable badge;
    ProgressDialog progressDialog;
    TextView shareTxt,tvQuantity;
    String itemId;
    Context ctx;
    Constant c;
    String[] urls;
    SliderView sliderView;
    SliderAdapterExample adapter;
    RecyclerView sizeList;
    SizeListAdapter sizeListAdapter;
    String sizeTxt="-",cartItemPricePositionText = "-";
    String[] sizeAray;
    boolean isSizeAvailable = false;
    String[] salePriceArray,mrpArray;
    TextView productNameTop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        ctx = this;

        itemId = getIntent().getExtras().getString("itemId");

        btNotify = findViewById(R.id.btNotify);
        bookNameTxt = findViewById(R.id.book_name);
        AuthNAmeTxt = findViewById(R.id.author_name);
        disTxt = findViewById(R.id.discription);
        costTxt = findViewById(R.id.cost_text);
        actualCost = findViewById(R.id.actual_cost_text);
        sliderView = findViewById(R.id.imageSlider);
        back_btn = findViewById(R.id.back_btn);
        addToCart = findViewById(R.id.addCartBtn);
        dicsountPercentage = findViewById(R.id.discount);
        buyNow =findViewById(R.id.buy_now);
        ratingTxt = findViewById(R.id.ratingTxt);
        sizeList = findViewById(R.id.size_list);
        deliveryTxt = findViewById(R.id.free_delivery_txt);
        tvQuantity = findViewById(R.id.tvQuantity);
        tvQuantity.setVisibility(View.GONE);
        cartBtn = findViewById(R.id.cart_btn);
        shareTxt = findViewById(R.id.share_btn);
        productNameTop = findViewById(R.id.productNameTop);
        publisherName = findViewById(R.id.publisher_name);

        sizeList.setHasFixedSize(true);
        sizeList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
//        wistlist_btn = findViewById(R.id.wistlist_btn);
        c = new Constant();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();

        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(this);
        }

       // badge.setCount(HomeActivity.getCartItemCount());
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSizeAvailable){
                    if (!sizeTxt.equals("-")){
                        progressDialog.show();
                        HashMap hm = new HashMap();
                        hm.put("uid",Constant.uid);
                        hm.put("itemId",itemId);
                        hm.put("quantity","1");
                        hm.put("size",sizeTxt);
                        hm.put("price",cartItemPricePositionText);

                        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
                            @Override
                            public void onEvent(String response) {
                                progressDialog.dismiss();
                                if (!response.equals("error")){
                                    onResume();
                                    Toast.makeText(ItemDetailActivity.this, "Added", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(ItemDetailActivity.this, "error", Toast.LENGTH_SHORT).show();
                                }

                            }
                        },hm);

                    }else {
                        Toast.makeText(ctx, "Select any size", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("uid",Constant.uid);
                    hm.put("itemId",itemId);
                    hm.put("quantity","1");
                    hm.put("size",sizeTxt);
                    hm.put("price",cartItemPricePositionText);


                    c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
                        @Override
                        public void onEvent(String response) {
                            progressDialog.dismiss();
                            if (!response.equals("error")){
                                onResume();
                                Toast.makeText(ItemDetailActivity.this, "Added", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ItemDetailActivity.this, "error", Toast.LENGTH_SHORT).show();
                            }

                        }
                    },hm);
                }

            }
        });

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSizeAvailable){
                    if (!sizeTxt.equals("-")){
                        progressDialog.show();
                        HashMap hm = new HashMap();
                        hm.put("uid",Constant.uid);
                        hm.put("itemId",itemId);
                        hm.put("quantity","1");
                        hm.put("size",sizeTxt);
                        hm.put("price",cartItemPricePositionText);

                        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
                            @Override
                            public void onEvent(String response) {
                                progressDialog.dismiss();
                                if (!response.equals("error")){
                                    onResume();
                                    startActivity(new Intent(ItemDetailActivity.this,CartActivity.class));
                                }else {
                                    Toast.makeText(ItemDetailActivity.this, "error", Toast.LENGTH_SHORT).show();
                                }

                            }
                        },hm);
                    }else {
                        Toast.makeText(ctx, "Select any size", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    progressDialog.show();
                    HashMap hm = new HashMap();
                    hm.put("uid",Constant.uid);
                    hm.put("itemId",itemId);
                    hm.put("quantity","1");
                    hm.put("size",sizeTxt);
                    hm.put("price",cartItemPricePositionText);

                    c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
                        @Override
                        public void onEvent(String response) {
                            progressDialog.dismiss();
                            if (!response.equals("error")){
                                onResume();
                                startActivity(new Intent(ItemDetailActivity.this,CartActivity.class));
                            }else {
                                Toast.makeText(ItemDetailActivity.this, "error", Toast.LENGTH_SHORT).show();
                            }

                        }
                    },hm);
                }

            }
        });


        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ItemDetailActivity.this,CartActivity.class));
            }
        });

        HashMap dhm = new HashMap();
        dhm.put("itemId",itemId);
        dhm.put("city",Constant.searchCityName);
        c.getDataFromServer(this, Request.Method.POST, "getSingleItemDetail", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                if (!response.equals("error"))
                {
                    try{
                        final HashMap hm = new HashMap();
                        JSONArray jsonArray = new JSONArray(response);
                        hm.put("itemname",jsonArray.getJSONObject(0).getString("itemname"));
                        hm.put("itemid",jsonArray.getJSONObject(0).getString("itemId"));
                        hm.put("image",jsonArray.getJSONObject(0).getString("image"));
                        hm.put("soldBy",jsonArray.getJSONObject(0).getString("soldBy"));
                        hm.put("username",Constant.userName);
                        hm.put("userid",Constant.uid);
                        hm.put("phone",Constant.userPhone);
                        hm.put("city",Constant.searchCityName);

                        bookNameTxt.setText(Constant.upperCaseWords(jsonArray.getJSONObject(0).getString("itemname")));
                        AuthNAmeTxt.setText(Constant.upperCaseWords(jsonArray.getJSONObject(0).getString("author")));
                        disTxt.setContent(jsonArray.getJSONObject(0).getString("description"));
                        disTxt.setTextMaxLength(300);
                        disTxt.setSeeMoreTextColor(R.color.colorPrimary);
                        disTxt.setNestedScrollingEnabled(false);
                        disTxt.setScrollContainer(false);
                        costTxt.setText("₹"+jsonArray.getJSONObject(0).getString("sale_price"));
                        actualCost.setText("₹"+jsonArray.getJSONObject(0).getString("mrp"));
                        dicsountPercentage.setText(setPersentageText(Float.parseFloat(jsonArray.getJSONObject(0).getString("sale_price")),Float.parseFloat(jsonArray.getJSONObject(0).getString("mrp")))+"% off");
                        productNameTop.setText(Constant.upperCaseWords(jsonArray.getJSONObject(0).getString("author")));

                        publisherName.setText(jsonArray.getJSONObject(0).getString("weight"));
                        urls = jsonArray.getJSONObject(0).getString("image").split("\\s+");

                        adapter = new SliderAdapterExample(ItemDetailActivity.this, urls);
                        String qty = jsonArray.getJSONObject(0).getString("quantity");
                        if(Integer.parseInt(qty)==0)
                        {
                            tvQuantity.setVisibility(View.VISIBLE);
                            tvQuantity.setTextColor(Color.RED);
                            tvQuantity.setText("OUT OF STOCK");
                            addToCart.setVisibility(View.GONE);
                            buyNow.setVisibility(View.GONE);
                            btNotify.setVisibility(View.VISIBLE);
                            btNotify.setOnClickListener(new View.OnClickListener() {
                                 @Override
                                 public void onClick(View view) {
                                     NotifyItems(hm);
                                 }
                             });
                         }
                         else if(Integer.parseInt(qty) <= 5)
                         {
                             tvQuantity.setVisibility(View.VISIBLE);
                             tvQuantity.setText("Only "+ qty +" Left");
                         }
                        //Glide.with(getApplicationContext()).load(jsonArray.getJSONObject(0).getString("feature_image")).into(imageView);

                        rating = String.valueOf(Float.parseFloat(jsonArray.getJSONObject(0).getString("rating")));

                        if (rating.equals("0.0") || rating.equals("0")){
                            ratingTxt.setText("No Ratings");
                        }else {
                            ratingTxt.setText(rating);
                        }

                        if (!Constant.deliveryCharge.equals("0")){
                            deliveryTxt.setText("Delivery Charge : ₹"+Constant.deliveryCharge);
                        }else {
                            deliveryTxt.setText("");
                            deliveryTxt.setVisibility(View.GONE);
                        }

                        if (!jsonArray.getJSONObject(0).getString("size").equals("-")){
                            isSizeAvailable = true;
                            sizeList.setVisibility(View.VISIBLE);
                            sizeAray = jsonArray.getJSONObject(0).getString("size").split(",");
                            sizeListAdapter = new SizeListAdapter(sizeAray,ItemDetailActivity.this,getApplicationContext(),-1);
                            sizeList.setAdapter(sizeListAdapter);

                            salePriceArray = jsonArray.getJSONObject(0).getString("sale_price_list").split(",");
                            mrpArray = jsonArray.getJSONObject(0).getString("mrp_list").split(",");
                        }else {
                            sizeList.setVisibility(View.GONE);
                            isSizeAvailable = false;
                        }

                        shareTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                shareLink();
                            }
                        });
                        sliderView.setSliderAdapter(adapter);

                        progressDialog.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                } else {

                    Toast.makeText(ctx, "this item is not available in this city.", Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();
            }
        },dhm);

    }

    private void NotifyItems(HashMap hm) {
        progressDialog.show();
        c.getDataFromServer(ItemDetailActivity.this,Request.Method.POST,"notifyItem", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                Log.d("notify Response", "onEvent: "+response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("status").equals("true"))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(ctx, "Your Request Has been Submitted", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(ctx, obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },hm);
    }


    public Uri createDynamicLink_Basic() {
        String link = "https://delivermybookdelivery.page.link/?itemId="+itemId+"&city="+Constant.searchCityName;
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://delivermybookdelivery.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.delivermybookdelivery.ios").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }

    public void shareLink() {
        Intent sendIntent = new Intent();
        String msg = "Buy this amazing item from deliver my book and get your item at your door step.  " + createDynamicLink_Basic();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public int setPersentageText(float numerator,float denominator) {
        int percentage = (int)(numerator * 100.0 / denominator + 0.5);
        return 100 - percentage;

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        new Constant().getDataFromServer(ctx, Request.Method.POST, "getCartItemCount", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Constant.CartItemCount = String.valueOf(Integer.valueOf(jsonArray.getJSONObject(0).getString("count"))+bookSetList.size());
                    badge.setCount(Constant.CartItemCount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },hm);

    }


    @Override
    protected void onStop() {
        super.onStop();

    }

    void countCartItem(){

    }

    @Override
    public void onItemClick(String size,int position) {
        sizeTxt = size;
        publisherName.setText(size);
        cartItemPricePositionText = String.valueOf(position);
        costTxt.setText("₹"+salePriceArray[position]);
        actualCost.setText("₹"+mrpArray[position]);
        sizeListAdapter = new SizeListAdapter(sizeAray,ItemDetailActivity.this,getApplicationContext(),position);
        sizeList.setAdapter(sizeListAdapter);
    }
}
