package com.grocico.grochery.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.fragments.SchoolListFragment;
import com.grocico.grochery.model.BookDetail;
import com.grocico.grochery.model.CartItemModel;
import com.grocico.grochery.model.CopyDetails;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SchoolSetAllDetails extends AppCompatActivity {

    Spinner spinner;
    String[] className = {"Nursery","KG1","KG2","Class 1","Class2","Class 3","Class 4","Class 5","Class 6","Class 7","Class 8","Class 9","Class 10",
            "Class 11th(Science)","Class 11(Commerce)","Class 11(Arts)","Class 11(Humanities)","Class 12th(Science)","Class 12(Commerce)","Class 12(Arts)","Class 12(Humanities)"};

    String[] item = {"Book Set","Copy Set"};
    FloatingActionButton addFab;
    String schoolID;
    Button addToCartBtn,bookSetBtn,copySetBtn;
    ProgressDialog progressDialog;
    DatabaseReference bookRef;
    RecyclerView bookList;
    int classSelectedId;
    DatabaseReference addCopyRef;
    TextView totalAmount,titleText;
    boolean isBook;
    boolean isCopyk;
    CheckBox withCoverCB;
    String withCover = "With Cover";
    int totalCopyCost = 0;
    int totalBookCost = 0;
    int lastSelectedSpinnerItemPosition = 0;
    String schoolName,classname;
    String bothsetText = "Books and Copy";
    String logoUrl = "https://firebasestorage.googleapis.com/v0/b/delivermybook-a6792.appspot.com/o/HomeScreenSliders%2FlogoTrans-min.png?alt=media&token=35da4160-51eb-4f7c-b089-1637a927c93a";
    ImageView cartBtn;

    int coverAmount = 0;
    TextView discountText;
    int promodiscount ;
    String excludedBook="";
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_set_all_details);
        addFab = findViewById(R.id.addFab);

            addFab.setVisibility(View.GONE);

        titleText = findViewById(R.id.titleText);
        cartBtn = findViewById(R.id.cart_btn);
        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();


        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(SchoolSetAllDetails.this,CartActivity.class));
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        spinner = findViewById(R.id.select_class);
        addToCartBtn = findViewById(R.id.add_to_cart);

        bookSetBtn = findViewById(R.id.book_set);
        copySetBtn = findViewById(R.id.copy_set);

        schoolID = getIntent().getExtras().getString("schoolID");
        schoolName = getIntent().getExtras().getString("schoolName");
        classname = getIntent().getExtras().getString("class");
        classSelectedId = getIntent().getExtras().getInt("classSelectedId");

        bookList = findViewById(R.id.bookList);

        bookList.setHasFixedSize(true);
        bookList.setLayoutManager(new LinearLayoutManager(this));


        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,className);

        spinner.setAdapter(aa);
        spinner.setSelection(classSelectedId);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (SchoolListFragment.classAvailable.contains(className[position])){
                    bookSetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_selected));
                    copySetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_normal));
                    refreshBookRecyclerView();
                    classname = className[position];
                    lastSelectedSpinnerItemPosition = position;
                }else {
                    spinner.setSelection(lastSelectedSpinnerItemPosition);
                    showSetUnAvailableDialog();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                refreshBookRecyclerView();
            }
        });




        bookSetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_selected));

        bookSetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleText.setText("Book Details");
                bookSetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_selected));
                copySetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_normal));
                refreshBookRecyclerView();
            }
        });

        copySetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshCopyRecyclerView();
                copySetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_selected));
                bookSetBtn.setBackgroundColor(getResources().getColor(R.color.tab_btn_normal));
                titleText.setText("Copy Details");
            }
        });


        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countCostOfBookSet();
            }
        });

    }

    private void showSetUnAvailableDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SchoolSetAllDetails.this);
        //builder.setTitle("Unavailable");
        builder.setMessage("Book/Copy set is not available for this class.");
        builder.show();
    }

    private void countCostOfBookSet() {
        totalBookCost = 0;
        isBook = true;
        bookRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :dataSnapshot.getChildren())
                {
                    if (ds.child("isBookAvailable").getValue().toString().equals("true")){
                        totalBookCost = totalBookCost + Integer.valueOf(ds.child("bookCost").getValue().toString());
                    }else {
                        excludedBook = excludedBook + ds.child("bookName").getValue().toString() +":"+ ds.child("bookCost").getValue().toString()+";";
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        countCostOfCopySet();
    }

    private void countCostOfCopySet() {

        totalCopyCost = 0;
        isCopyk = true;
        addCopyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :dataSnapshot.getChildren())
                {
                    if (ds.child("isCopyAvailable").getValue().toString().equals("true")){
                        totalCopyCost = totalCopyCost + Integer.valueOf(ds.child("copyCost").getValue().toString());
                    }

                }
               // totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost-getFivePer(totalBookCost+totalCopyCost)+coverAmount));
                totalAmount.setText(String.valueOf(totalCopyCost+totalBookCost+coverAmount));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        placeOrderDialog();
    }

    private void placeOrderDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View place_order_dialog = inflater.inflate(R.layout.place_order_dialog_layout,null);
        builder.setView(place_order_dialog);
        final CheckBox bookCb = place_order_dialog.findViewById(R.id.booksetCb);
        CheckBox copyCb = place_order_dialog.findViewById(R.id.copysetCb);
        withCoverCB = place_order_dialog.findViewById(R.id.withCoverCB);
        totalAmount = place_order_dialog.findViewById(R.id.totalAmount);
        Button place_order_btn = place_order_dialog.findViewById(R.id.place_order_btn);
        Button cancel = place_order_dialog.findViewById(R.id.cancel);
        discountText = place_order_dialog.findViewById(R.id.discount_text);
        Button btPromoCode = place_order_dialog.findViewById(R.id.btPromoCode);
        final EditText etPromoCode = place_order_dialog.findViewById(R.id.etPromoCode);
        final LinearLayout linPromo = place_order_dialog.findViewById(R.id.linePromo);
        final TextView promotext = place_order_dialog.findViewById(R.id.tvpromotext);
        bookCb.setChecked(true);
        copyCb.setChecked(true);
        withCoverCB.setChecked(true);
        withCoverCB.setVisibility(View.VISIBLE);
        final AlertDialog alertDialog = builder.create();
        withCover = "With Cover";
        promodiscount = 0;


        btPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String promocode = etPromoCode.getText().toString().trim();
                if(!promocode.isEmpty())
                {
                    HashMap hm = new HashMap();
                    hm.put("code",promocode);
                    hm.put("uid",Constant.uid);
                    new Constant().getDataFromServer(SchoolSetAllDetails.this, Request.Method.POST, "checkPromo", new Constant.OnResponseFromServer() {
                        @Override
                        public void onEvent(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                if(obj.get("status").equals("true") && obj.get("message").equals("applied"))
                                {
                                    promodiscount = Integer.parseInt(obj.getString("discount"));
                                    int discount =  totalBookCost+totalCopyCost+coverAmount;
                                    Log.d("discount", "------------->>>>> "+discount);
                                    discountText.setText("Discount : "+promodiscount+"%");
                                    totalAmount.setText(String.valueOf(discount-((discount * promodiscount)/100)));
                                 //   Toast.makeText(SchoolSetAllDetails.this, ""+promodiscount, Toast.LENGTH_SHORT).show();
                                    promotext.setTextColor(Color.GREEN);
                                    promotext.setText("Promo Code applied successfully");
                                    Constant.isPromoCodeApplied="true";
                                    Constant.promoCodeApplied = promocode;
                                    etPromoCode.setEnabled(false);
                                    etPromoCode.setClickable(false);

                                   // Toast.makeText(SchoolSetAllDetails.this, ""+isPromoCodeApplied +" "+promoCodeApplied, Toast.LENGTH_SHORT).show();
                                }
                                else if(obj.get("status").equals("used"))
                                {
                                 promotext.setTextColor(Color.RED);
                                 promotext.setText(obj.getString("message"));
                                }
                                else if(obj.get("status").equals("invalid"))
                                {
                                    promotext.setTextColor(Color.RED);
                                    promotext.setText(obj.getString("message"));
                                }
                                else
                                {
                                    Toast.makeText(SchoolSetAllDetails.this, obj.getString("status")+"/"+ obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },hm);
                }
                else {
                    etPromoCode.setError("Please enter a code");
                }
            }
        });
        withCoverCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b){
                    withCover = "With Cover";
                    if (bothsetText.equals("Books and Copy")){
                       // totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost-getFivePer(totalBookCost+totalCopyCost)+coverAmount));
                        totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost+coverAmount));
                    }
                }
                else {
                    withCover = "Without Cover";
                    //totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost-getFivePer(totalBookCost+totalCopyCost)));
                    totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost));
                }

            }
        });

        bookCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                discountText.setText("");
                etPromoCode.setText("");
                etPromoCode.setEnabled(true);
                promotext.setText("");

                if (isChecked){
                    isBook = true;
                    if (isCopyk){
                        if (withCover.equals("With Cover")){
                            withCoverCB.setVisibility(View.VISIBLE);
                            //totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost-getFivePer(totalBookCost+totalCopyCost)+coverAmount));
                            totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost+coverAmount));
                        }else {
                            withCoverCB.setVisibility(View.VISIBLE);
                           // totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost-getFivePer(totalBookCost+totalCopyCost)));
                            totalAmount.setText(String.valueOf(totalBookCost+totalCopyCost+coverAmount));
                        }

                        bothsetText = "Books and Copy";
                    }else {
                        withCover = "Without Cover";
                        withCoverCB.setVisibility(View.GONE);
                       // totalAmount.setText(String.valueOf(totalBookCost-getTwoPer(totalBookCost)));
                        totalAmount.setText(String.valueOf(totalBookCost));
                    }
                }else {
                    isBook = false;
                    withCover = "Without Cover";
                    withCoverCB.setVisibility(View.GONE);
                    if (isCopyk){
                        bothsetText = "Only Copy";
                       // totalAmount.setText(String.valueOf(totalCopyCost-getTwoPer(totalCopyCost)));

                        totalAmount.setText(String.valueOf(totalCopyCost));
                    }else {
                        bothsetText = "No Item";
                        totalAmount.setText("0.0");
                        discountText.setText("");
                    }
                }

                if(isBook && isCopyk)
                {
                    linPromo.setVisibility(View.VISIBLE);
                }

                else
                {
                    linPromo.setVisibility(View.GONE);
                }
            }
        });

        copyCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                discountText.setText("");
                etPromoCode.setText("");
                etPromoCode.setEnabled(true);
                promotext.setText("");

                if (isChecked){
                    isCopyk = true;
                    if (isBook){
                        bothsetText = "Books and Copy";
                        if (withCover.equals("With Cover")){
                            withCoverCB.setVisibility(View.VISIBLE);
                          //  totalAmount.setText(String.valueOf(totalCopyCost+totalBookCost-getFivePer(totalBookCost+totalCopyCost)+coverAmount));
                            totalAmount.setText(String.valueOf(totalCopyCost+totalBookCost+coverAmount));
                        }else {
                            withCoverCB.setVisibility(View.VISIBLE);
                          //  totalAmount.setText(String.valueOf(totalCopyCost+totalBookCost-getFivePer(totalBookCost+totalCopyCost)));
                            totalAmount.setText(String.valueOf(totalCopyCost+totalBookCost+coverAmount));
                        }

                    }else {
                        withCover = "Without Cover";
                        withCoverCB.setVisibility(View.GONE);
                        //totalAmount.setText(String.valueOf(totalCopyCost-getTwoPer(totalCopyCost)));
                        totalAmount.setText(String.valueOf(totalCopyCost));
                        bothsetText = "Only Copy";
                    }
                }else {
                    isCopyk = false;
                    withCover = "Without Cover";
                    withCoverCB.setVisibility(View.GONE);
                    if (isBook){
                     //   totalAmount.setText(String.valueOf(totalBookCost-getTwoPer(totalBookCost)));
                        totalAmount.setText(String.valueOf(totalBookCost));
                        bothsetText = "Only Books";
                    }else {
                        bothsetText = "No Item";
                        totalAmount.setText("0.0");
                        discountText.setText("");
                    }
                }
                if(isBook && isCopyk)
                {
                    linPromo.setVisibility(View.VISIBLE);
                }
                else
                {
                    linPromo.setVisibility(View.GONE);
                }
            }
        });

        place_order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                hTotal = totalAmount.getText().toString();
//                //   String.valueOf()bookCb.isChecked()
//
//                int total = Integer.valueOf(totalAmount.getText().toString());
                if (!bothsetText.equals("No Item")){
                    addProductToCart(getApplicationContext(),schoolName,classname+"/"+bothsetText+"/"+withCover,totalAmount.getText().toString(),logoUrl,bookRef,schoolID+classname);

                    alertDialog.dismiss();
                }else {
                    Toast.makeText(SchoolSetAllDetails.this, "please select any item", Toast.LENGTH_SHORT).show();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Constant.isPromoCodeApplied="false";
            }
        });



        alertDialog.show();
    }


    private int getTwoPer(int val){
        int i = (val*2)/100;
        discountText.setText("Discount Amount : ₹"+ String.valueOf(i)+" (2%)");
        return i;
    }

    private int getFivePer(int val){
        int i = (val*5)/100;
        discountText.setText("Discount Amount : ₹"+ String.valueOf(i)+" (5%)");
        return i;
    }


    public void addProductToCart(final Context context, final String name, final String brand, final String cost, final String photo, final DatabaseReference productRef, final String id)
    {

        CartItemModel cartItemModel = new CartItemModel();
        if (!cartItemModel.isItemAvailableInCart(id)){
            cartItemModel.setItemname(Constant.upperCaseWords(brand));
            cartItemModel.setItemId(id);
            cartItemModel.setMrp(String.valueOf(Integer.valueOf(cost)+(Math.round((Integer.valueOf(cost)*10)/100))));
            cartItemModel.setSale_price(cost);
            cartItemModel.setFeature_image(photo);
            cartItemModel.setQuantity("1");
            cartItemModel.setCategory("bookcopyset");
            if (excludedBook.equals("")){
                cartItemModel.setSub_category("-");
            }else {
                cartItemModel.setSub_category(excludedBook);
            }

            cartItemModel.setWeight("-");
            cartItemModel.setAuthor(name);
            cartItemModel.setSize("-");
            cartItemModel.setColors("-");
            cartItemModel.setSoldBy("ditsjbp");
            cartItemModel.setRating("-");
            cartItemModel.setCartItemSize("-");
            cartItemModel.setCartItemQuantity("1");
            MainActivity.bookSetList.add(cartItemModel);
        }
        Toast.makeText(context, "Added", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshBookRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void refreshCopyRecyclerView() {
        addCopyRef = Constant.schoolListRef.child(schoolID).child("copy_list").child(spinner.getSelectedItem().toString().trim().replace(" ","_"));
        FirebaseRecyclerOptions<CopyDetails> options =
                new FirebaseRecyclerOptions.Builder<CopyDetails>()
                        .setQuery(addCopyRef, CopyDetails.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<CopyDetails, CopyViewHolder>(options) {
            @Override
            public CopyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_copy_detail_layout, parent, false);

                return new CopyViewHolder(view);
            }
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            protected void onBindViewHolder(CopyViewHolder viewHolder, int position, final CopyDetails copyDetails) {
                viewHolder.setCopyType(copyDetails.getCopyType());
                viewHolder.setPageCount(copyDetails.getPagesCount());
                viewHolder.setPrice(copyDetails.getCopyCost());
                viewHolder.setIsCopyAvailable(copyDetails.getIsCopyAvailable());
                final String copyID =getRef(position).getKey();

            }
        };
        adapter.startListening();
        bookList.setAdapter(adapter);
    }


    private void refreshBookRecyclerView(){
        bookRef = Constant.schoolListRef.child(schoolID).child("book_list").child(spinner.getSelectedItem().toString().trim().replace(" ","_"));
        addCopyRef = Constant.schoolListRef.child(schoolID).child("copy_list").child(spinner.getSelectedItem().toString().trim().replace(" ","_"));

        FirebaseRecyclerOptions<BookDetail> options =
                new FirebaseRecyclerOptions.Builder<BookDetail>()
                        .setQuery(bookRef, BookDetail.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<BookDetail, BooksViewHolder>(options) {
            @Override
            public BooksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_book_detail_layout, parent, false);

                return new BooksViewHolder(view);
            }
            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            protected void onBindViewHolder(BooksViewHolder viewHolder, int position, final BookDetail bookDetail) {

                viewHolder.setBookName(bookDetail.getBookName());
                viewHolder.setPublisher(bookDetail.getPublisher());
                viewHolder.setPrice(bookDetail.getBookCost());
                viewHolder.setIsBookAvailable(bookDetail.getIsBookAvailable());

                final String bookID =getRef(position).getKey();


            }
        };
        adapter.startListening();
        bookList.setAdapter(adapter);
    }



    public static class BooksViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public BooksViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }
        public void setBookName(String item)
        {
            TextView book_name= mView.findViewById(R.id.school_name);
            book_name.setText(item);

        }
        public void setPublisher(String item)
        {
            TextView publisher= mView.findViewById(R.id.publisher);
            publisher.setText(item);
        }

        public void setPrice(String item)
        {
            TextView book_price= mView.findViewById(R.id.book_price);
            book_price.setText("Rs."+item);
        }

        public void setIsBookAvailable(String item){
            TextView tv = mView.findViewById(R.id.notAvailableTxt);

            if (item.equals("true")){
                tv.setVisibility(View.INVISIBLE);
            }
            else {
                tv.setVisibility(View.VISIBLE);
            }
        }

    }

    public static class CopyViewHolder extends RecyclerView.ViewHolder{
        View mView;

        public CopyViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setPageCount(String item)
        {
            TextView pages_count= mView.findViewById(R.id.pagesCountView);
            pages_count.setText(item);
        }
        public void setCopyType(String item)
        {
            TextView copyType= mView.findViewById(R.id.copyTypeView);
            copyType.setText(item);

        }

        public void setPrice(String item)
        {
            TextView copy_cost= mView.findViewById(R.id.copyCostView);
            copy_cost.setText("Rs."+item);
        }

        public void setIsCopyAvailable(String item){
            TextView tv = mView.findViewById(R.id.notAvailableTxt);

            if (item.equals("true")){
                tv.setVisibility(View.INVISIBLE);
            }
            else {
                tv.setVisibility(View.VISIBLE);
            }
        }
    }
}

