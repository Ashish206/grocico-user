package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.ItemsGridAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.ItemsGridModel;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ShopByStoreItemActivity extends AppCompatActivity implements ItemsGridAdapter.AddToCartBtnListener {

    ApiInterface apiInterface;
    RecyclerView rcv;
    ProgressDialog pd;
    String shopName,shopCode;

    String city= Constant.searchCityName;
    TextView noItemTxt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_by_store_item);
        shopName = getIntent().getExtras().getString("shopName");
        shopCode = getIntent().getExtras().getString("shopCode");

        rcv= findViewById(R.id.subCatrecv);
        rcv.setHasFixedSize(true);
        rcv.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);

        noItemTxt = findViewById(R.id.noItemTxt);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Constant.upperCaseWords(shopName));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pd.show();
        loadData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void loadData()
    {
        getShopItems().enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, retrofit2.Response<List<ItemsGridModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                ItemsGridAdapter itemsGridAdapter = new ItemsGridAdapter(getApplicationContext(),response.body(),ShopByStoreItemActivity.this);
                rcv.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {
                pd.dismiss();
                noItemTxt.setVisibility(View.VISIBLE);
//                Toast.makeText(ItemListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }


        });
    }

    private Call<List<ItemsGridModel>> getShopItems() {
        return apiInterface.getItemByShopCode(
                shopCode
        );
    }

    @Override
    public void onClick(final String itemId) {
        pd.show();
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        hm.put("itemId",itemId);
        hm.put("quantity","1");

        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                pd.dismiss();
                if (!response.equals("error")){
                    Toast.makeText(ShopByStoreItemActivity.this, "Added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(ShopByStoreItemActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

            }
        },hm);
    }
}
