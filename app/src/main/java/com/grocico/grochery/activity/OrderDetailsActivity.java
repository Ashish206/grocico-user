package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.grocico.grochery.R;
import com.grocico.grochery.adapter.OrderDetailsAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity implements OrderDetailsAdapter.OnClick{
    RecyclerView historyList;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    public static HashMap infoHashMap;
    TextView noItemTxt;
    private ApiInterface movieService;
    public static boolean referashState = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        historyList = findViewById(R.id.historyList);
        infoHashMap = new HashMap();
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        historyList.setLayoutManager(linearLayoutManager);
        historyList.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        movieService = ApiClient.getClient().create(ApiInterface.class);

        noItemTxt = findViewById(R.id.noItemTxt);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Constant.upperCaseWords("Order History"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog.show();
        loadAllOrders();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (referashState){
            loadAllOrders();
            referashState = false;
        }
    }

    private void loadAllOrders() {
        getAllOrders().enqueue(new Callback<ArrayList<OrderModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderModel>> call, Response<ArrayList<OrderModel>> response) {
                noItemTxt.setVisibility(View.INVISIBLE);
                progressDialog.dismiss();
                Log.d("------>",String.valueOf(response.body().get(0).getUserId()));
                OrderDetailsAdapter orderDetailsAdapter = new OrderDetailsAdapter(getApplicationContext(),response.body(),OrderDetailsActivity.this);
                historyList.setAdapter(orderDetailsAdapter);

            }

            @Override
            public void onFailure(Call<ArrayList<OrderModel>> call, Throwable t) {
                t.printStackTrace();
                noItemTxt.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        });

    }


    private Call<ArrayList<OrderModel>> getAllOrders() {
        return movieService.getUserOrder(
                Constant.uid
        );
    }


    @Override
    public void onClick(OrderModel model) {
        Intent i = new Intent(OrderDetailsActivity.this,PurchaseDetailActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.putExtra("Cost",model.getTotalAmount());
        i.putExtra("ActualCost",model.getActualTotalAmount());
        i.putExtra("date",model.getDate());
        i.putExtra("delivered",model.getDeliveryStatus());
        i.putExtra("orderKey",model.getOrder_id());
        i.putExtra("paymentMethod",model.getPaymentMethod());
        i.putExtra("userAddress",model.getUserAddress());
        i.putExtra("userId",model.getUserId());
        i.putExtra("userName",model.getUserName());
        i.putExtra("userPhone",model.getUserPhone());
        i.putExtra("userPostalCode",model.getUserPostalCode());
        i.putExtra("deliveryCharge",model.getDeliveryCharge());
        i.putExtra("referralAmount",model.getReferralAmount());
        i.putExtra("interestAmount",model.getInterest());
        startActivity(i);
        overridePendingTransition(0,0);
    }
}

