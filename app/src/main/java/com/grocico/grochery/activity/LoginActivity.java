package com.grocico.grochery.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    EditText nameField,emailField;
    Button registerBtn;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Spinner citySpinner;
    String [] cityArray = {"Select Your City","Jabalpur"};
//    String [] cityArray = {"Jabalpur","Bhopal","Indor","Katni"};
     //sliderLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nameField = findViewById(R.id.nameField);
        emailField = findViewById(R.id.emailField);
        registerBtn = findViewById(R.id.register_btn);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        sharedPreferences = getSharedPreferences("admin",MODE_PRIVATE);
        editor = sharedPreferences.edit();

        citySpinner = findViewById(R.id.cityField);


        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,cityArray);

        citySpinner.setAdapter(aa);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(nameField.getText().toString())){
                    nameField.setError("Empty");
                    nameField.requestFocus();
                }else if (TextUtils.isEmpty(emailField.getText().toString())){
                    emailField.setError("Empty");
                    emailField.requestFocus();
                }
//                else if (citySpinner.getSelectedItem().toString().equals("Select Your City")){
//                    Toast.makeText(LoginActivity.this, "Select Any City", Toast.LENGTH_SHORT).show();
//                }
                else {
                    attemptUserLoginViaFirebase();
                }
            }
        });
    }

    private void attemptUserLoginViaFirebase() {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK) {
                getTokenId();
            } else {
                progressDialog.dismiss();
                Toast.makeText(this, "Authentication Failed", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void getTokenId() {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("------>", "getInstanceId failed", task.getException());
                            return;
                        }else {


                            final String token = task.getResult().getToken();
                            progressDialog.show();
                            HashMap hm = new HashMap();
                            hm.put("email",emailField.getText().toString());
                            hm.put("name",nameField.getText().toString());
                            hm.put("phone", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                            hm.put("tokenId",token);

                            RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.BASE_URL + "addUserDetails", new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    try{
                                        JSONObject job = new JSONObject(response);
                                        if (job.getString("status").equals("1") || job.getString("status").equals("2")){
                                            editor.putString("name",nameField.getText().toString().toLowerCase());
                                            editor.putString("email",emailField.getText().toString().toLowerCase());
                                            editor.putString("phone",FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                                            editor.putString("tokenId",token);
                                            editor.putString("city",citySpinner.getSelectedItem().toString().toLowerCase());
                                            editor.apply();
                                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                            finish();
                                        }
                                        else {
                                            FirebaseAuth.getInstance().getCurrentUser().delete();
                                        }

                                    }
                                    catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    progressDialog.dismiss();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    Log.d("RESPONSE:>>> ", error.toString() + "<<<");
                                    Toast.makeText(LoginActivity.this, ")pps !! " + error.toString(), Toast.LENGTH_SHORT).show();

                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> jsonParams = new ArrayMap<>();
                                    jsonParams.put("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    jsonParams.put("email", emailField.getText().toString().toLowerCase());
                                    jsonParams.put("name", nameField.getText().toString().toLowerCase());
                                    jsonParams.put("phone", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                                    jsonParams.put("tokenId", token);
                                    jsonParams.put("city", "Jabalpur");
                                    return jsonParams;
                                }

                                @Override
                                public String getBodyContentType() {
                                    return "application/x-www-form-urlencoded";
                                }
                            };

                            queue.add(stringRequest);

                        }


                    }
                });

    }
}

