package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.PurchaseDetailAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.model.PurchaseDetailModel;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseDetailActivity extends AppCompatActivity implements PurchaseDetailAdapter.CancelOrder,PurchaseDetailAdapter.Rating {
    ArrayList<PurchaseDetailModel> bookDetailList;
    public static String Cost,ActualCost,date,delivered,orderKey,paymentMethod,userAddress,userId,userName,userPhone,userPostalCode,orderIdUser,deliveryCharge,referralAmount,interest;
    RecyclerView history_detail;
    LinearLayoutManager linearLayoutManager;
    public static int count = 0;
    ProgressDialog progressDialog;
    float avg;
    private ApiInterface movieService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_detail);

        getSupportActionBar().setTitle("Order Details");

        Cost = getIntent().getExtras().getString("Cost");
        ActualCost = getIntent().getExtras().getString("ActualCost");
        date = getIntent().getExtras().getString("date");
        delivered = getIntent().getExtras().getString("delivered");
        orderKey = getIntent().getExtras().getString("orderKey");

        paymentMethod = getIntent().getExtras().getString("paymentMethod");
        userAddress = getIntent().getExtras().getString("userAddress");
        userId = getIntent().getExtras().getString("userId");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userPostalCode = getIntent().getExtras().getString("userPostalCode");
        deliveryCharge = getIntent().getExtras().getString("deliveryCharge");
        referralAmount = getIntent().getExtras().getString("referralAmount");
        interest = getIntent().getExtras().getString("interestAmount");

        history_detail = findViewById(R.id.history_detail);
        history_detail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        movieService = ApiClient.getClient().create(ApiInterface.class);
        linearLayoutManager = new LinearLayoutManager(this);
        history_detail.setLayoutManager(linearLayoutManager);
        history_detail.setHasFixedSize(true);
        bookDetailList = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        loadHistoryDetails();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadHistoryDetails() {
        getPurchaseFromOrder().enqueue(new Callback<ArrayList<PurchaseDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PurchaseDetailModel>> call, Response<ArrayList<PurchaseDetailModel>> response) {
                progressDialog.dismiss();
                bookDetailList = response.body();
                count = bookDetailList.size();
                PurchaseDetailAdapter orderDetailsAdapter = new PurchaseDetailAdapter(getApplicationContext(),bookDetailList,PurchaseDetailActivity.this,PurchaseDetailActivity.this);
                orderDetailsAdapter.setData(bookDetailList);

                history_detail.setAdapter(orderDetailsAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<PurchaseDetailModel>> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
            }
        });

    }

    private Call<ArrayList<PurchaseDetailModel>> getPurchaseFromOrder() {
        return movieService.getPurchaseFromOrder(
                orderKey
        );
    }

    @Override
    public void onClacelOrderClick(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseDetailActivity.this);
        builder.setMessage("Do you really want to cancel this order?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        progressDialog.show();
                        HashMap hm = new HashMap();
                        hm.put("deliveryStatus","Cancelled");
                        hm.put("orderId",key);
                        Constant c = new Constant();
                        c.getDataFromServer(PurchaseDetailActivity.this, Request.Method.POST, "changeOrderStatus", new Constant.OnResponseFromServer() {
                            @Override
                            public void onEvent(String response) {
                                finish();
                                OrderDetailsActivity.referashState = true;
                                progressDialog.dismiss();
                                Toast.makeText(PurchaseDetailActivity.this, "Order Cancelled", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        },hm);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();


    }


    @Override
    public void onRateingListener(String itemId, float rate, String order_id) {
        progressDialog.show();
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("itemId",itemId);
        hm.put("rate",String.valueOf(rate));
        hm.put("order_id",order_id);
        c.getDataFromServer(this, Request.Method.POST, "setRating", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                if (!response.equals("error")){
                    progressDialog.dismiss();
                }
            }
        },hm);
    }
}
