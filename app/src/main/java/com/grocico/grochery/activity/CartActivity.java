package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.CartAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.extra.Notification;
import com.grocico.grochery.extra.jsonparse;
import com.grocico.grochery.fragments.PaymentFragment;
import com.grocico.grochery.model.CartItemModel;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;

import static com.grocico.grochery.activity.MainActivity.bookSetList;
import static com.grocico.grochery.extra.Constant.deliveryCharge;
import static com.grocico.grochery.extra.Constant.interestAmount;

public class CartActivity extends AppCompatActivity implements CartAdapter.ItemListener,CartAdapter.RemoveCartItem,CartAdapter.EditCartItem, PaytmPaymentTransactionCallback {
    String GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    int GOOGLE_PAY_REQUEST_CODE = 123;
    RecyclerView cartList;
    int i = 0;
    long cartItemCount = 0;
    ArrayList<CartItemModel> cartListArray;
    CartAdapter cartAdapter;
    LinearLayout noItemText;
    public static TextView TotalAmount;
    CardView card;
    public static float totalAmount;
    public static float totalActualAmount;
    Button shopNowBtn;
    public static Button proceedBtn;
    int screen = 0;
    PaymentFragment paymentFragment;
    FragmentManager fragmentManager;
    public static ProgressDialog progressDialog;
    long childrenCount = 0;
    int loopCount = 0;
    String paymentMethod = "";
    int orderStage = 0;

    String keyDb;
    String order_id;
    String orderTitle="";
    private PaymentsClient mPaymentsClient;
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 42;
    Constant c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Constant.useableReferralAmount = 0;
        cartList = findViewById(R.id.cartList);
        cartList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        noItemText = findViewById(R.id.noItemTxt);
        TotalAmount = findViewById(R.id.totalText);
        shopNowBtn = findViewById(R.id.shop_now_btn);
        shopNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        card= findViewById(R.id.card);

        cartListArray = new ArrayList();
        c = new Constant();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        mPaymentsClient =
                Wallet.getPaymentsClient(
                        this,
                        new Wallet.WalletOptions.Builder()
                                .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                                .build());

        proceedBtn = findViewById(R.id.proceedBtn);
        proceedBtn.setEnabled(false);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Constant.userAddress.equals("")){
                    if (proceedBtn.getText().toString().toLowerCase().equals("proceed next")){
                        paymentFragment = new PaymentFragment();
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().add(R.id.userDetailFragment,paymentFragment).commit();
                        screen++;
                        proceedBtn.setText("Place Order");
                    }else {
                        if (Constant.paymentMethod!= -1){
                            c.getDataFromServer(getApplicationContext(), Request.Method.GET, "getOrderId", new Constant.OnResponseFromServer() {
                                @Override
                                public void onEvent(String response) {
                                    if (!response.equals("error")){
                                        try {
                                            JSONObject job = new JSONObject(response);
                                            order_id = job.getString("order_id");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        if (Constant.paymentMethod == 0){
                                            paymentMethod = "Google Pay";
                                            try{

                                                googlePay((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
                                            }catch (Exception e){
                                                Toast.makeText(CartActivity.this, "Google pay not found in your phone", Toast.LENGTH_SHORT).show();
                                            }

                                        }else if (Constant.paymentMethod == 1){
                                            paymentMethod = "Paytm";
                                            try{
                                                sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
                                                dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                            }catch (Exception e){
                                                Toast.makeText(CartActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                            }

                                        }else if (Constant.paymentMethod == 2){
                                            paymentMethod = "Cash On Delivery";
                                            orderPlaced();
                                        }
                                        else if (Constant.paymentMethod == 3){
                                            paymentMethod = "EMI";
                                            interestAmount = Math.round((CartActivity.totalAmount*8)/100);
                                            orderPlaced();
                                        }
                                    }
                                }
                            },null);

                        }else {
                            Toast.makeText(CartActivity.this, "Select Payment Method", Toast.LENGTH_SHORT).show();
                        }
                    }

                }else {
                    startActivity(new Intent(CartActivity.this,ChangeAddressActivity.class));
                }

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        loadCartList();
    }

    void loadCartList(){
        progressDialog.show();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        c.getDataFromServer(this, Request.Method.POST, "getCartItemCount", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    cartListArray.clear();
                    cartItemCount = Integer.valueOf(jsonArray.getJSONObject(0).getString("count") +bookSetList.size());
                    if (cartItemCount!=0) {
                        totalAmount = 0;
                        totalActualAmount = 0;
                        card.setVisibility(View.VISIBLE);
                        noItemText.setVisibility(View.GONE);
                        cartList.setVisibility(View.VISIBLE);

                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);
                        Call<ArrayList<CartItemModel>> call = apiService.getCartItem(Constant.uid);
                        call.enqueue(new Callback<ArrayList<CartItemModel>>() {
                            @Override
                            public void onResponse(Call<ArrayList<CartItemModel>> call, retrofit2.Response<ArrayList<CartItemModel>> response) {
                                cartListArray = response.body();
                                cartListArray.addAll(bookSetList);
                                orderTitle = "";
                                for (int i = 0;i<cartListArray.size();i++){

                                    if (!cartListArray.get(i).getCartItemSize().equals("-")){
                                        CartItemModel cartItemModel = cartListArray.get(i);
                                        cartItemModel.setSale_price(cartListArray.get(i).getSale_price_list().split(",")[Integer.valueOf(cartListArray.get(i).getCartItemPricePosition())]);
                                        cartItemModel.setMrp(cartListArray.get(i).getMrp_list().split(",")[Integer.valueOf(cartListArray.get(i).getCartItemPricePosition())]);

                                        cartListArray.set(i,cartItemModel);
                                    }
                                    orderTitle +=cartListArray.get(i).getItemname() +", ";
                                    totalAmount = totalAmount + (Float.parseFloat(cartListArray.get(i).getSale_price()) * Float.parseFloat(cartListArray.get(i).getCartItemQuantity()));
                                    totalActualAmount = totalActualAmount + (Float.parseFloat(cartListArray.get(i).getMrp()) * Float.parseFloat(cartListArray.get(i).getCartItemQuantity()));
                                }
                                setData();

                            }

                            @Override
                            public void onFailure(Call<ArrayList<CartItemModel>> call, Throwable t) {
                                cartListArray.clear();
                                if (bookSetList.size()!=0){
                                    cartListArray.addAll(bookSetList);
                                    orderTitle = "";
                                    for (int i = 0;i<cartListArray.size();i++){
                                        orderTitle +=cartListArray.get(i).getItemname() +", ";
                                        totalAmount = totalAmount + (Float.parseFloat(cartListArray.get(i).getSale_price()) * Float.parseFloat(cartListArray.get(i).getCartItemQuantity()));
                                        totalActualAmount = totalActualAmount + (Float.parseFloat(cartListArray.get(i).getMrp()) * Float.parseFloat(cartListArray.get(i).getCartItemQuantity()));
                                    }
                                    setData();
                                }
                                progressDialog.dismiss();
                            }
                        });

                    }else{
                        progressDialog.dismiss();
                        card.setVisibility(View.GONE);
                        cartList.removeAllViews();
                        cartList.setVisibility(View.GONE);
                        noItemText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },hm);
    }

    private void setData() {
        cartList.removeAllViews();
        cartAdapter = new CartAdapter(getApplicationContext(),cartListArray,CartActivity.this);
        cartAdapter.setData(cartListArray);
        cartList.setAdapter(cartAdapter);
        cartAdapter.notifyDataSetChanged();
        if (totalAmount!=0 && totalAmount>=250 && Constant.referralAmount>=10){
            if (((Constant.referralAmount*20)/100)>=200){
                Constant.useableReferralAmount = 200;
            }else {
                Constant.useableReferralAmount = ((Constant.referralAmount*20)/100);
            }

        }else {
            Constant.useableReferralAmount=0;
        }

        if (totalAmount>=500){
            deliveryCharge = "0";
        }else {
            deliveryCharge = "29";
        }

        int x =  (int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount);
        TotalAmount.setText("₹"+x);
    }

    @Override
    public void onBackPressed() {
        if (screen == 0){
            super.onBackPressed();
        }
        else {
            screen--;
            Constant.paymentMethod = -1;
            fragmentManager.beginTransaction().remove(paymentFragment).commit();
            proceedBtn.setText("Proceed Next");
        }

    }

    @Override
    public void onItemClick(String key, ImageView imageView) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Constant.breaker = 1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.breaker = 1;
        setData();
    }

    @Override
    public void onRemoveItemClick(String key,String category) {
        progressDialog.show();

        if (!category.equals("bookcopyset")){
            HashMap hm = new HashMap();
            hm.put("uid",Constant.uid);
            hm.put("itemId",key);

            c.getDataFromServer(this, Request.Method.POST, "deleteCartItem", new Constant.OnResponseFromServer() {
                @Override
                public void onEvent(String response) {
                    try {
                        JSONObject job = new JSONObject(response);
                        if (job.getString("status").equals("1")){
                            //cartList.removeAllViews();
                            loadCartList();

                            Toast.makeText(CartActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Log.d("--->","error "+job.getString("message"));
                        }
                    }catch (Exception e){
                        Log.d("--->","error "+e.getMessage());
                    }
                }
            },hm);
        }else {
            for (int p = 0; p < bookSetList.size();p++){
                if (bookSetList.get(p).getItemId().equals(key)){
                    bookSetList.remove(p);
                    loadCartList();
                    Toast.makeText(CartActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                }
            }
        }



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onEditCartItemListener(final String key, final String category,String quantity) {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        d.setTitle("Edit Order");
        d.setMessage("Change the quantity of your product");
        d.setView(dialogView);
        final NumberPicker numberPicker = dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(Integer.valueOf(quantity));
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });
        d.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.show();

                if (!category.equals("bookcopyset")){
                    HashMap hm = new HashMap();
                    hm.put("uid",Constant.uid);
                    hm.put("itemId",key);
                    hm.put("quantity",String.valueOf(numberPicker.getValue()));
                    c.getDataFromServer(getApplicationContext(), Request.Method.POST, "editCartItemQuantity", new Constant.OnResponseFromServer() {
                        @Override
                        public void onEvent(String response) {
                            try {
                                JSONObject job = new JSONObject(response);
                                if (job.getString("status").equals("1")){

                                    loadCartList();
                                    Toast.makeText(CartActivity.this, "Item Edited", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Log.d("--->","error "+job.getString("message"));
                                }
                            }catch (Exception e){
                                Log.d("--->","error "+e.getMessage());
                            }
                        }
                    },hm);
                }else {
                    for (int p = 0;p< bookSetList.size();p++){

                        if (bookSetList.get(p).getItemId().equals(key)){
                            CartItemModel c = bookSetList.get(p);
                            c.setCartItemQuantity(String.valueOf(numberPicker.getValue()));
                            bookSetList.set(p,c);
                            loadCartList();
                            Toast.makeText(CartActivity.this, "Item Edited", Toast.LENGTH_SHORT).show();
                        }
                    }
                }



            }
        });
        d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alertDialog = d.create();
        alertDialog.show();

    }

    private void googlePay(int total) {

//        GooglePay.totalGooglePayAmount = String.valueOf(total);
//        final Optional<JSONObject> isReadyToPayJson = GooglePay.getIsReadyToPayRequest();
//        if (!isReadyToPayJson.isPresent()) {
//            return;
//        }
//        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.get().toString());
//        if (request == null) {
//            return;
//        }
//        Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
//        task.addOnCompleteListener(
//                new OnCompleteListener<Boolean>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Boolean> task) {
//                        try {
//                            boolean result = task.getResult(ApiException.class);
//                            if (result) {
//                                requestPayment();
//                            }
//                        } catch (ApiException exception) {
//                            // handle developer errors
//                            Log.d("-------->",exception.getMessage());
//                        }
//                    }
//                });

        //paytmqr281005050101809ywssbyv31@paytm

        //paytmqr281005050101i2ar71p8vfsc@paytm
        Uri uri =
                new Uri.Builder()
                        .scheme("upi")
                        .authority("pay")
                        .appendQueryParameter("pa", "trikkihex@okaxis")
                        .appendQueryParameter("pn", "Grocico")
                        .appendQueryParameter("tr", String.valueOf(randInt(10,10)))
                        .appendQueryParameter("tn", "Grocico")
                        .appendQueryParameter("am", String.valueOf(total))
                        .appendQueryParameter("cu", "INR")
                        .build();


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        intent.setPackage(GOOGLE_PAY_PACKAGE_NAME);
        startActivityForResult(intent, GOOGLE_PAY_REQUEST_CODE);
    }

//    public void requestPayment() {
//        Optional<JSONObject> paymentDataRequestJson = GooglePay.getPaymentDataRequest();
//        if (!paymentDataRequestJson.isPresent()) {
//            return;
//        }
//        PaymentDataRequest request =
//                PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString());
//        if (request != null) {
//            AutoResolveHelper.resolveTask(
//                    mPaymentsClient.loadPaymentData(request), this, LOAD_PAYMENT_DATA_REQUEST_CODE);
//        }
//    }


    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        String json = paymentData.toJson();
                        // if using gateway tokenization, pass this token without modification
                        JSONObject paymentMethodData = null;
                        String paymentToken = null;
                        try {
                            paymentMethodData = new JSONObject(json)
                                    .getJSONObject("paymentMethodData");
                            paymentToken = paymentMethodData
                                    .getJSONObject("tokenizationData")
                                    .getString("token");
                            orderPlaced();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case Activity.RESULT_CANCELED:
                        showOrderUnSuccessfulDialog();
                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        showOrderUnSuccessfulDialog();
                        // Log the status for debugging.
                        // Generally, there is no need to show an error to the user.
                        // The Google Pay payment sheet will present any account errors.
                        break;
                    default:
                        // Do nothing.
                }
                break;
            default:
                // Do nothing.
        }

        if (requestCode == GOOGLE_PAY_REQUEST_CODE) {
            try{
                if (data.getStringExtra("Status").toLowerCase().equals("failure")){
                    showOrderUnSuccessfulDialog();
                }
                else {
                    orderPlaced();
                }
            }catch (Exception e){
                showOrderUnSuccessfulDialog();
                e.printStackTrace();
            }

        }
    }

    public void updateQuantity(String itemId, String quantity)
    {
        HashMap hm = new HashMap();
        hm.put("itemid",itemId);
        hm.put("quantity",quantity);
          c.getDataFromServer(this, Request.Method.POST, "updateQuantity", new Constant.OnResponseFromServer() {
              @Override
              public void onEvent(String response) {
                  Log.d("-->", "updateQuantity------------>"+response);
              }
          },hm);
    }

    void orderPlaced(){

        progressDialog.show();

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        final String today = formatter.format(Calendar.getInstance().getTime());

        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
        final String date = dateFormat.format(Calendar.getInstance().getTime());

        DateFormat timeForamt = new SimpleDateFormat("h:mm a");
        final String time = timeForamt.format(Calendar.getInstance().getTime());



        final HashMap hm = new HashMap();
        hm.put("order_id",order_id);
        hm.put("date",DateFormat.getDateInstance(DateFormat.MEDIUM).format(Calendar.getInstance().getTime()));
        hm.put("time",time);
        hm.put("orderTitle",orderTitle);
        hm.put("deliveryStatus","Order Placed");
        hm.put("userId",Constant.uid);
        hm.put("userName",Constant.userName.replace("'",""));
        hm.put("userPhone",Constant.userPhone);
        hm.put("userState",Constant.stateName.replace("'",""));
        hm.put("userCity",Constant.cityName.replace("'",""));
        hm.put("userAddress",Constant.userAddress.replace("'",""));
        hm.put("userLandmark",Constant.landMark.replace("'",""));
        hm.put("userPostalCode",Constant.postalCode);
        hm.put("userLat",Constant.userLat);
        hm.put("userTokenId",Constant.userToken);
        hm.put("filterDate",today);
        hm.put("userLng",Constant.userLng);
        hm.put("paymentMethod",paymentMethod);
        hm.put("deliveryBoyName","not selected");
        hm.put("deliveryBoyNumber","not selected");
        hm.put("deliveryBoyId","not selected");
        hm.put("referralAmount",String.valueOf(Constant.useableReferralAmount));
        hm.put("deliveryCharge",String.valueOf(Integer.valueOf(Constant.deliveryCharge)));
        hm.put("interest", String.valueOf(interestAmount));
        hm.put("TotalAmount",String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)+ interestAmount));
        hm.put("ActualTotalAmount",String.valueOf((int)totalActualAmount));



        c.getDataFromServer(this, Request.Method.POST, "addNewOrder", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                try {
                    JSONObject job = new JSONObject(response);
                    if (job.getString("status").equals("1")){
                        addPurches();
                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(CartActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },hm);
    }

    private void addPurches(){
        final HashMap hm2 = new HashMap();
        hm2.put("order_id",order_id);
        hm2.put("itemname",cartListArray.get(loopCount).getItemname().replace("'",""));
        hm2.put("feature_image",cartListArray.get(loopCount).getFeature_image());
        hm2.put("category",cartListArray.get(loopCount).getCategory());
        hm2.put("sub_category",cartListArray.get(loopCount).getSub_category());
        hm2.put("mrp",cartListArray.get(loopCount).getMrp());
        hm2.put("sale_price",cartListArray.get(loopCount).getSale_price());
        hm2.put("quantity",cartListArray.get(loopCount).getCartItemQuantity());
        hm2.put("weight",cartListArray.get(loopCount).getWeight());
        hm2.put("author",cartListArray.get(loopCount).getAuthor().replace("'",""));
        hm2.put("size",cartListArray.get(loopCount).getCartItemSize());
        hm2.put("colors",cartListArray.get(loopCount).getColors());
        hm2.put("soldBy",cartListArray.get(loopCount).getSoldBy());
        hm2.put("itemId",cartListArray.get(loopCount).getItemId());
        hm2.put("rated","false");
        if (!cartListArray.get(loopCount).getCategory().equals("bookcopyset")){
            updateQuantity(cartListArray.get(loopCount).getCartItemId(),cartListArray.get(loopCount).getCartItemQuantity());
        }
        Log.d("------->",String.valueOf(hm2));

        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addOrderDetailsToPurchase", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                Log.d("--------->",response+"/"+loopCount+"/"+cartListArray.size());
                loopCount++;

                if (loopCount==cartListArray.size()){
                   // hm.clear();
                    hm2.clear();
                    orderPlacedSuccessfully();
                }else {
                    addPurches();
                }
            }
        },hm2);
    }

    private void orderPlacedSuccessfully() {
            newOrderMsg();
            new Notification(this).sendNotificationToSubs("New Order Alert","You have received a new order. Please accept it.","admin");
            sendSms("Your Order Of Rs+"+String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount))+" has been placed. Thanks for shopping with us.");
            HashMap hm = new HashMap();
            hm.put("uid",Constant.uid);
            c.getDataFromServer(this, Request.Method.POST, "deleteAllItemFromCart", new Constant.OnResponseFromServer() {
                @Override
                public void onEvent(String response) {
                }
            },hm);

            //-------Updating promoCode(if applied) in users table
        if(Constant.isPromoCodeApplied.equals("true") && Constant.promoCodeApplied != "" && MainActivity.bookSetList.size()!=0 )
        {
            String  code = Constant.promoCodeApplied;

            HashMap hm1 = new HashMap();
            hm1.put("code",code);
            hm1.put("uid",Constant.uid);

         new Constant().getDataFromServer(CartActivity.this, Request.Method.POST, "updatePromoCodeInUsersTable", new Constant.OnResponseFromServer() {
             @Override
             public void onEvent(String response) {
                 try {
                     JSONObject obj = new JSONObject(response);
                     if(obj.get("status").equals("true"))
                     {
                         //Toast.makeText(CartActivity.this, "Promo code benefits Applied", Toast.LENGTH_SHORT).show();
                     }
                     else
                     {
                         //Toast.makeText(CartActivity.this, ""+obj.get("message"), Toast.LENGTH_SHORT).show();
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

             }
         },hm1);
        }

        progressDialog.dismiss();
        Constant.useableReferralAmount = 0;
        showOrderSuccessfulDialog();


    }

    private void showOrderSuccessfulDialog() {
        bookSetList.clear();
        interestAmount = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.order_successful,null,false);
        builder.setView(v);
        builder.setCancelable(false);
        builder.setPositiveButton("Continue Shopping", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            Constant.paymentMethod =-1;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showOrderUnSuccessfulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.order_unsuccessful_dialog,null,false);
        builder.setView(v);
        builder.setCancelable(false);
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void sendSms(String smstext) {
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=MyDITS&route=4&mobiles="+Constant.userPhone+"&authkey=106940A18cEbcPcZ75c503f65&message="+smstext+
                " Will reach you shortly";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("-------->",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("-------->",error.toString());
                //Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }

    void newOrderMsg(){
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=MyDITS&route=4&mobiles=9993358037,9179079696&authkey=106940A18cEbcPcZ75c503f65&message=New Delivery Alert!!";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String> {
        private ProgressDialog dialog = new ProgressDialog(CartActivity.this);
        //private String orderId , mid, custid, amt;
        String url ="http://grocico.in/paytm/generateChecksum.php";
        String varifyurl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        // String varifyurl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        // "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID"+orderId;
        String CHECKSUMHASH ="";
        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        protected String doInBackground(ArrayList<String>... alldata) {
            jsonparse jsonParser = new jsonparse(CartActivity.this);
            String param=
                    "MID="+Constant.mid+"&ORDER_ID=" + order_id+
                            "&CUST_ID="+Constant.uid+
                            "&CHANNEL_ID=WAP&TXN_AMOUNT="+(int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)+"&WEBSITE=DEFAULT"+
                            "&CALLBACK_URL=https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+order_id+"&INDUSTRY_TYPE_ID=Retail";
            JSONObject jsonObject = jsonParser.makeHttpRequest(url,"POST",param);
            // yaha per checksum ke saht order id or status receive hoga..
            Log.e("CheckSum result >>",jsonObject.toString());
            if(jsonObject != null){
                Log.e("CheckSum result >>",jsonObject.toString());
                try {
                    CHECKSUMHASH=jsonObject.has("CHECKSUMHASH")?jsonObject.getString("CHECKSUMHASH"):"";
                    Log.e("CheckSum result >>",CHECKSUMHASH);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return CHECKSUMHASH;
        }
        @Override
        protected void onPostExecute(String result) {
            Log.e(" setup acc ","  signup result  " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            PaytmPGService Service = PaytmPGService.getProductionService();
            // when app is ready to publish use production service
            // PaytmPGService  Service = PaytmPGService.getProductionService();
            // now call paytm service here
            //below parameter map is required to construct PaytmOrder object, Merchant should replace below map values with his own values
            HashMap<String, String> paramMap = new HashMap<String, String>();
            //these are mandatory parameters
            paramMap.put("MID", Constant.mid); //MID provided by paytm
            paramMap.put("ORDER_ID", order_id);
            paramMap.put("CUST_ID", Constant.uid);
            paramMap.put("CHANNEL_ID", "WAP");
            paramMap.put("TXN_AMOUNT", String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)));
            paramMap.put("WEBSITE", "DEFAULT");
            paramMap.put("CALLBACK_URL" ,"https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+order_id);
            //paramMap.put( "EMAIL" , "abc@gmail.com");   // no need
            // paramMap.put( "MOBILE_NO" , "9144040888");  // no need
            paramMap.put("CHECKSUMHASH" ,CHECKSUMHASH);
            //paramMap.put("PAYMENT_TYPE_ID" ,"CC");    // no need
            paramMap.put("INDUSTRY_TYPE_ID", "Retail");
            PaytmOrder Order = new PaytmOrder(paramMap);
            Log.e("checksum ", "param "+ paramMap.toString());
            Service.initialize(Order,null);
            // start payment service call here
            Service.startPaymentTransaction(CartActivity.this, true, true,
                    CartActivity.this  );
        }
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        Log.e("checksum-- ", " respon true " + bundle.toString());
        if (bundle.getString("STATUS").equals("TXN_SUCCESS")){
            orderPlaced();
        }else {
            Toast.makeText(this, bundle.getString("RESPMSG"), Toast.LENGTH_SHORT).show();
            showOrderUnSuccessfulDialog();
        }

    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "No Network Available", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Log.e("checksum-- ", " ui fail respon  "+ s );
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Log.e("checksum-- ", " error loading pagerespon true "+ s + "  s1 " + s1);
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Log.e("checksum-- ", " cancel call back respon  " );
        showOrderUnSuccessfulDialog();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Log.e("checksum-- ", "  transaction cancel " );
        showOrderUnSuccessfulDialog();
    }
}
