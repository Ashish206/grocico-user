package com.grocico.grochery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.grocico.grochery.R;
import com.grocico.grochery.fragments.SchoolListFragment;

public class SchoolSetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_set);
        Fragment mFragment = new SchoolListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.schoolFragment, mFragment).commit();
    }
}
