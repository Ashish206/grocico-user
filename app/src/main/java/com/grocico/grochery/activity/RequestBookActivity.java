package com.grocico.grochery.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class RequestBookActivity extends AppCompatActivity {
    TextInputEditText bookName,publisherName,description;
    Button submitBtn;
    private Uri resultUri;
    private ImageView profileImage;
    ProgressDialog progressDialog;
    public static final int REQUEST_IMAGE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_book);
        bookName = findViewById(R.id.bookName);
        publisherName = findViewById(R.id.publisher_name);
        description = findViewById(R.id.description);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        profileImage = findViewById(R.id.image);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Request For Book");

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStoragePermissionGranted()) {
                    onProfileImageClick();
                }
            }
        });
        submitBtn = findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(description.getText().toString())){
                    if (resultUri != null) {
                        progressDialog.show();

                        final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("request_image").child(resultUri.getLastPathSegment());

                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), resultUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        try {
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        byte[] data = baos.toByteArray();
                        UploadTask uploadTask = filePath.putBytes(data);

                        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }
                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();
                                    progressDialog.show();
                                    DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                                    final String date = dateFormat.format(Calendar.getInstance().getTime());
                                    final HashMap hm = new HashMap();

                                    hm.put("image",downloadUri.toString());
                                    hm.put("description",description.getText().toString());
                                    hm.put("name", Constant.userName);
                                    hm.put("email",Constant.userEmail);
                                    hm.put("phone",Constant.userPhone);
                                    hm.put("uid",Constant.uid);
                                    hm.put("token",Constant.userToken);
                                    hm.put("date",date);
                                    final String key =  Constant.requestBookRef.push().getKey();

                                    Constant.requestBookRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                        @Override
                                        public void onComplete(@NonNull Task task) {
                                            if (task.isSuccessful()){
                                                hm.put("isCompleted","Not Completed");
//                                                Constant.userRef.child("Requests").child(key).updateChildren(hm);
                                                description.setText("");
                                                progressDialog.dismiss();
                                                Toast.makeText(RequestBookActivity.this, "Request Submitted!! We will Notify you shortly.", Toast.LENGTH_LONG).show();
                                                //newRequestMsg();
                                                finish();
                                            }else {
                                                Toast.makeText(RequestBookActivity.this, "Request Failed!!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });


                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Oops/" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else {

                        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                        final String date = dateFormat.format(Calendar.getInstance().getTime());
                        final HashMap hm = new HashMap();

                        hm.put("image","null");
                        hm.put("description",description.getText().toString());
                        hm.put("name", Constant.userName);
                        hm.put("email",Constant.userEmail);
                        hm.put("phone",Constant.userPhone);
                        hm.put("uid",Constant.uid);
                        hm.put("token",Constant.userToken);
                        hm.put("date",date);
                        final String key =  Constant.requestBookRef.push().getKey();

                        Constant.requestBookRef.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()){
                                    hm.put("isCompleted","Not Completed");
//                                    Constant.userRef.child("Requests").child(key).updateChildren(hm);
//                                    bookName.setText("");
//                                    publisherName.setText("");
                                    description.setText("");
                                    Toast.makeText(RequestBookActivity.this, "Request Submitted!! We will Notify you shortly.", Toast.LENGTH_LONG).show();
                                    //newRequestMsg();
                                }else {
                                    Toast.makeText(RequestBookActivity.this, "Request Failed!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                }else {
                    Toast.makeText(RequestBookActivity.this, "Select any image or/and write a description. !!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    void newRequestMsg(){
        String url = "http://api.msg91.com/api/sendhttp.php?country=91&sender=MyDITS&route=4&mobiles=9993358037&authkey=106940A18cEbcPcZ75c503f65&message=New Request Alert!!";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                resultUri = data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    profileImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("-->","Permission is granted");
                return true;
            } else {

                Log.v("-->","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("-->","Permission is granted");
            return true;
        }
    }



    void onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        } else {
                            // TODO - handle permission denied case
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(RequestBookActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(RequestBookActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
