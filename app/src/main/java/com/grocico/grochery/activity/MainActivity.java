package com.grocico.grochery.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.grocico.grochery.BuildConfig;
import com.grocico.grochery.R;
import com.grocico.grochery.adapter.BestSellerAdapter;
import com.grocico.grochery.adapter.HomeInfiniteAdapter;
import com.grocico.grochery.adapter.HomeMenuAdapter;
import com.grocico.grochery.adapter.ShopListAdapter;
import com.grocico.grochery.adapter.ViewPagerAdapter;
import com.grocico.grochery.adapter.brandStoreAdapter;
import com.grocico.grochery.adapter.newArrivalAdapter;
import com.grocico.grochery.extra.ApiClient;
import com.grocico.grochery.extra.ApiInterface;
import com.grocico.grochery.extra.Constant;
import com.grocico.grochery.extra.CountDrawable;
import com.grocico.grochery.fragments.SearchResultFragment;
import com.grocico.grochery.model.CartItemModel;
import com.grocico.grochery.model.HomeInfiniteModel;
import com.grocico.grochery.model.HomeMenuModel;
import com.grocico.grochery.model.ItemsGridModel;
import com.grocico.grochery.model.SearchResultModel;
import com.grocico.grochery.model.ShopDetailsModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements BestSellerAdapter.AddToCartBtnListener, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ShopListAdapter.ItemListener, newArrivalAdapter.AddToCartBtnListener, HomeMenuAdapter.ItemListener,brandStoreAdapter.ItemListener,HomeInfiniteAdapter.OnBannerClick {

    String category;
    Uri shopuri;
    String[] classNameArr = {"Select","Nursery","KG1","KG2","Class 1","Class2","Class 3","Class 4","Class 5","Class 6","Class 7","Class 8","Class 9","Class 10",
            "Class 11th(Science)","Class 11(Commerce)","Class 11(Arts)","Class 11(Humanities)","Class 12th(Science)","Class 12(Commerce)","Class 12(Arts)","Class 12(Humanities)"};

    String[] choiceArr = {"Select","Book Set","Copy Set","Both"};
    String shop_url;
    FirebaseAuth mAUth;
    RelativeLayout splash_screen;
    TextView userNameTv,userPhoneTv;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context ctx;
    ImageView nav_toggle_btn;
    ArrayList slidersUrl;
    int i = 0;
    ProgressDialog pd;
    Timer timer;
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    CardView school_set_card;
    ImageView cartBtn,shopimage;
    EditText feedback;
    static CountDrawable badge;
    RecyclerView shopList,bestSellerList,newArrivalList;
    ApiInterface apiInterface;
    public static ArrayList<CartItemModel> bookSetList;
    CardView requestForBook,vendorBtn,preBookBtn;
    String isUpdateAvailable = "no";
    String versionName = BuildConfig.VERSION_NAME;
    String mVersionName = "";
    RecyclerView hmenu;
    ArrayList hmArray;
    RecyclerView brandStoreList,infiniteList;
    ArrayList brandStoreArray;
    ImageView bn1Iv,bnr2Iv,bnr3Iv;
    String urlBnr1,urlBnr2,urlBnr3;
    Fragment gridFragment;
    int x=0;
    FragmentManager fragmentManager;
    public static SearchView searchView;
    private SimpleCursorAdapter mAdapter;
    private ApiInterface movieService;
    private ArrayList<SearchResultModel> SUGGESTIONS;
    View gridShowView;
    FrameLayout frameLayout;
    TextView titleText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       pd=new ProgressDialog(this);
       pd.setMessage("Please Wait...");
       pd.setTitle("Saving Info");
       pd.setCancelable(false);
        ctx = this;
        nav_toggle_btn = findViewById(R.id.nav_toggle_btn);
        slidersUrl = new ArrayList();
        nav_toggle_btn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                DrawerLayout navDrawer = findViewById(R.id.drawer_layout);
                // If the navigation drawer is not open then open it, if its already open then close it.
                if(!navDrawer.isDrawerOpen(Gravity.START)) navDrawer.openDrawer(Gravity.START);
                else navDrawer.closeDrawer(Gravity.END);
            }
        });
        mAUth = FirebaseAuth.getInstance();
        splash_screen = findViewById(R.id.splash_screen);
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                splash_screen.setVisibility(View.GONE);
            }
        }, 3000);
        if (mAUth.getCurrentUser() != null) {

            Constant.rootRef = FirebaseDatabase.getInstance().getReference();
            sharedPreferences = getSharedPreferences("admin", MODE_PRIVATE);
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);
            userNameTv = header.findViewById(R.id.txtUserName);
            userPhoneTv = header.findViewById(R.id.txtUserNumber);
            hmenu = findViewById(R.id.recyclerView);
            hmenu.setNestedScrollingEnabled(false);
            hmArray = new ArrayList();

            infiniteList = findViewById(R.id.infiniteList);
            infiniteList.setHasFixedSize(true);
            infiniteList.setNestedScrollingEnabled(false);
            infiniteList.setLayoutManager(new LinearLayoutManager(this));

            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            brandStoreList = findViewById(R.id.shopByBrand);
            brandStoreArray = new ArrayList();

            hmArray.add(new HomeMenuModel("Grocery And Staples", R.drawable.grocery_staples, "#ffffff"));
            hmArray.add(new HomeMenuModel("Biscuits And Snacks", R.drawable.biscuits_snacks, "#ffffff"));
            hmArray.add(new HomeMenuModel("Breakfast And Dairy", R.drawable.breakfast_dairy, "#ffffff"));
            hmArray.add(new HomeMenuModel("Beverages", R.drawable.beverage, "#ffffff"));
            hmArray.add(new HomeMenuModel("Frozen Food", R.drawable.frozen_food, "#ffffff"));
            hmArray.add(new HomeMenuModel("Personal Care", R.drawable.personal_care, "#ffffff"));
            hmArray.add(new HomeMenuModel("House Holds", R.drawable.household_items, "#ffffff"));
            hmArray.add(new HomeMenuModel("Chocolates", R.drawable.chocolates, "#ffffff"));
            hmArray.add(new HomeMenuModel("Baby Care", R.drawable.baby_kids, "#ffffff"));

            @SuppressLint("WrongConstant") GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
            hmenu.setLayoutManager(manager);
            hmenu.setDrawingCacheEnabled(true);
            hmenu.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);



            HomeMenuAdapter adapter = new HomeMenuAdapter(this, hmArray, this);
            hmenu.setAdapter(adapter);


            @SuppressLint("WrongConstant") GridLayoutManager manager2 = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
            brandStoreList.setLayoutManager(manager2);
            brandStoreList.setDrawingCacheEnabled(true);
            brandStoreList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


            brandStoreArray.add(new HomeMenuModel("Britannia", R.drawable.bs1, "#ffffff"));
            brandStoreArray.add(new HomeMenuModel("AAshirwaad", R.drawable.bs2, "#ffffff"));
            brandStoreArray.add(new HomeMenuModel("Cadbury", R.drawable.bs3, "#ffffff"));
            brandStoreArray.add(new HomeMenuModel("MDH", R.drawable.bs4, "#ffffff"));
            brandStoreArray.add(new HomeMenuModel("Mortein", R.drawable.bs5, "#ffffff"));
            brandStoreArray.add(new HomeMenuModel("Patanjali", R.drawable.bs6, "#ffffff"));

            bn1Iv = findViewById(R.id.bnr1);
            bnr2Iv = findViewById(R.id.bnr2);
            bnr3Iv = findViewById(R.id.bnr3);


            searchView = findViewById(R.id.search_bar);
            SUGGESTIONS = new ArrayList();
            movieService = ApiClient.getClient().create(ApiInterface.class);
            gridShowView = findViewById(R.id.grid_show_view);
            frameLayout = findViewById(R.id.frame2);
            frameLayout.setVisibility(View.GONE);
            titleText = findViewById(R.id.titleText);



            brandStoreAdapter adapter2 = new brandStoreAdapter(this, brandStoreArray, this);
            brandStoreList.setAdapter(adapter2);

            requestForBook = findViewById(R.id.requestForBook);
            requestForBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,RequestBookActivity.class));
                }
            });

            preBookBtn = findViewById(R.id.prebook_btn);
            preBookBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    preBookDialog();
                }
            });

            vendorBtn = findViewById(R.id.vendor_btn);
            vendorBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    becomeAMemberDialog();
                }
            });

            bookSetList = new ArrayList<>();
            Constant.uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

            Constant.userName  = sharedPreferences.getString("name","");
            Constant.userEmail = sharedPreferences.getString("email","");
            Constant.userToken = sharedPreferences.getString("tokenId","");
            Constant.userPhone = sharedPreferences.getString("phone","");

            Constant.cityName = sharedPreferences.getString("city","");

            Constant.landMark   = sharedPreferences.getString("landmark","");
            Constant.stateName  = sharedPreferences.getString("state","");
            Constant.postalCode = sharedPreferences.getString("postalCode","");
            Constant.userAddress= sharedPreferences.getString("address","");
            Constant.userLat    = sharedPreferences.getString("lat","");
            Constant.userLng    = sharedPreferences.getString("lng","");

            Constant.schoolListRef = Constant.rootRef.child("Products").child(Constant.capitalizeFirstLetter(Constant.searchCityName)).child("AllSchoolList");
            Constant.deliveryCharge = "20";
            Constant.requestBookRef = Constant.rootRef.child("Requests").child(Constant.searchCityName);
            Constant.updateUrlRef = Constant.rootRef.child("AppUpdates");
            checkUpdate();
            userNameTv.setText(Constant.upperCaseWords(Constant.userName));
            userPhoneTv.setText(Constant.userPhone);
            viewPager = findViewById(R.id.viewPagerHome);
            final int[] currentPage = {0,1,2};
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                int i=0;
                public void run() {
                    if (i == 3) {
                        i = 0;
                    }
                    viewPager.setCurrentItem(currentPage[i] ,true);
                    i++;
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);

            FirebaseDatabase.getInstance().getReference().child("HomeScreenSlider").child("Jabalpur").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds :dataSnapshot.getChildren()){
                        slidersUrl.add(ds.child("sliderImageUrl").getValue().toString());
                        i++;
                    }
                    viewPagerAdapter = new ViewPagerAdapter(MainActivity.this,slidersUrl);
                    viewPager.setAdapter(viewPagerAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            Constant.rootRef.child("Banners").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    urlBnr1 = dataSnapshot.child("banner1").getValue().toString();
                    urlBnr2 = dataSnapshot.child("banner2").getValue().toString();
                    urlBnr3 = dataSnapshot.child("banner3").getValue().toString();
                    Glide.with(getApplicationContext()).load(urlBnr1).into(bn1Iv);
                    Glide.with(getApplicationContext()).load(urlBnr2).into(bnr2Iv);
                    Glide.with(getApplicationContext()).load(urlBnr3).into(bnr3Iv);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

//            school_set_card = findViewById(R.id.school_set_card);
//            school_set_card.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    startActivity(new Intent(MainActivity.this,SchoolSetActivity.class));
//                }
//            });


            shopList= findViewById(R.id.shopList);
            shopList.setHasFixedSize(true);
            shopList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

            bestSellerList= findViewById(R.id.bestSellersList);
            bestSellerList.setHasFixedSize(true);
            bestSellerList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

            newArrivalList= findViewById(R.id.newArrivalList);
            newArrivalList.setHasFixedSize(true);
            newArrivalList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));


            cartBtn = findViewById(R.id.cart_btn);
            cartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this,CartActivity.class));
                }
            });
            LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
            if (reuse != null && reuse instanceof CountDrawable) {
                badge = (CountDrawable) reuse;
            } else {
                badge = new CountDrawable(this);
            }


            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_group_count, badge);


            fragmentManager = getSupportFragmentManager();

            final String[] from = new String[] {"filter"};
            final int[] to = new int[] {android.R.id.text1};
            mAdapter = new SimpleCursorAdapter(this,
                    R.layout.single_search_item,
                    null,
                    from,
                    to,
                    CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


            searchView.setSuggestionsAdapter(mAdapter);


            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionSelect(int i) {

                    return false;
                }

                @Override
                public boolean onSuggestionClick(int i) {
                    Cursor cursor = (Cursor) mAdapter.getItem(i);
                    String txt = cursor.getString(cursor.getColumnIndex("filter"));
                    searchView.setQuery(txt, true);
                    // Toast.makeText(HomeActivity.this, txt, Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            gridShowView.requestFocus();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    // getFinalSearchResult(s);
                    Constant.searchKey = s;
//                    titleText.setText(Constant.upperCaseWords(s));
                    if (x==0){

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                        gridShowView.setVisibility(View.GONE);
                        searchView.requestFocus();
                        gridFragment = new SearchResultFragment();
                        x=1;
                        fragmentManager.beginTransaction().replace(R.id.frame2,gridFragment)
                                .commit();
                        frameLayout.setVisibility(View.VISIBLE);
                    }

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    try{
                        getSearchResultFromApi(s);

                    }catch (Exception e){

                    }

                    return false;
                }
            });


            int searchCloseButtonId = searchView.getContext().getResources()
                    .getIdentifier("android:id/search_close_btn", null, null);
            ImageView closeButton =  this.searchView.findViewById(searchCloseButtonId);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    searchView.setQuery(null,false);
                    gridShowView.setVisibility(View.VISIBLE);
                    searchView.clearFocus();
//                    titleText.setText(Constant.upperCaseWords(getIntent().getStringExtra("category")));
                    try {
                        fragmentManager.beginTransaction().remove(gridFragment).commit();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    frameLayout.setVisibility(View.GONE);
                    x=0;

                }
            });


            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(final PendingDynamicLinkData pendingDynamicLinkData) {
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                            }
                            if (deepLink != null && deepLink.getBooleanQueryParameter("itemId",false)){
                                String bookid = deepLink.getQueryParameter("itemId");
                                Intent i = new Intent(MainActivity.this,ItemDetailActivity.class);
                                i.putExtra("itemId",bookid);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        }
                    });
            //loadShop();
            loadHomeScreen();
            loadBestSeller();
            loadNewArrival();
        }else {
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
        }

    }


    private void getSearchResultFromApi(String key){
        SUGGESTIONS.clear();
        getSearch(key).enqueue(new Callback<List<SearchResultModel>>() {
            @Override
            public void onResponse(Call<List<SearchResultModel>> call, Response<List<SearchResultModel>> response) {
                for(SearchResultModel s : response.body()){
                    SUGGESTIONS.add(s);
                }
                Collections.reverse(SUGGESTIONS);
                populateAdapter();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SearchResultModel>> call, Throwable t) {
                t.printStackTrace();
                SUGGESTIONS.clear();
                populateAdapter();
                mAdapter.notifyDataSetChanged();

            }
        });
    }


    private void populateAdapter() {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "filter" });
        for (int i=0; i<SUGGESTIONS.size(); i++) {
            c.addRow(new Object[] {i, SUGGESTIONS.get(i).getSearchResult()});
        }
        mAdapter.changeCursor(c);
    }

    private Call<List<SearchResultModel>> getSearch(String searchKey) {
        return movieService.getSearch(
                searchKey
        );
    }

    @Override
    public void onBackPressed() {
        if (x>0){
            gridShowView.setVisibility(View.VISIBLE);
            searchView.clearFocus();
            searchView.setQuery("",false);
            fragmentManager.beginTransaction().remove(gridFragment).commit();
            frameLayout.setVisibility(View.GONE);
            x=0;
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.nav_cart) {
            startActivity(new Intent(MainActivity.this,CartActivity.class));
        } else if (id == R.id.nav_history) {
            startActivity(new Intent(MainActivity.this,OrderDetailsActivity.class));
        }  else if (id == R.id.nav_share) {
            shareApp();
        } else if (id == R.id.nav_contact_us) {
            contactUs();
        }else if (id == R.id.nav_rate) {
            rateThisApp();
        }else if (id == R.id.nav_feedback) {
            FeedBack();
        }else if (id == R.id.nav_signout) {
            signoutBox();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void loadShop()
    {
        getShopList().enqueue(new Callback<ArrayList<ShopDetailsModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ShopDetailsModel>> call, Response<ArrayList<ShopDetailsModel>> response) {
                ShopListAdapter shopListAdapter = new ShopListAdapter(getApplicationContext(),response.body(),MainActivity.this);
                shopList.setAdapter(shopListAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<ShopDetailsModel>> call, Throwable t) {
                Log.d("------->",""+t.getMessage());
            }
        });
    }

    private Call<ArrayList<ShopDetailsModel>> getShopList() {
        return apiInterface.getAllShopDetails(
                sharedPreferences.getString("city","")
        );
    }


    private void contactUs() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.contact_us_dialog,null,false);
        LinearLayout mailUs = v.findViewById(R.id.mailUsLayout);
        LinearLayout callUs = v.findViewById(R.id.callUsLayout);

        mailUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("message/rfc822");
                in.putExtra(Intent.EXTRA_EMAIL  , new String[]{"dits.delivermybook@gmail.com"});
                in.putExtra(Intent.EXTRA_SUBJECT, "");
                in.putExtra(Intent.EXTRA_TEXT   , "");
                in.setPackage("com.google.android.gm");

                try {
                    startActivity(Intent.createChooser(in, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this, "There are no email clients installed.",Toast.LENGTH_SHORT).show();
                }
            }
        });


        callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+917247243888"));
                startActivity(intent);
            }
        });

        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        AlertDialog ad=abd.create();
        ad.show();
    }

    private void signoutBox()
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Do you want to sign out?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        signOut();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void FeedBack() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.feedback_layout,null,false);
        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setTitle("Feedback");
        abd.setCancelable(false);
        abd.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feedback = v.findViewById(R.id.feedtext);
                String feedbackstr = feedback.getText().toString().trim();
                if (feedbackstr.isEmpty()) {
                    Toast.makeText(MainActivity.this, "write something", Toast.LENGTH_SHORT).show();
                } else {
                    Intent in = new Intent(Intent.ACTION_SEND);
                    in.setType("message/rfc822");
                    in.putExtra(Intent.EXTRA_EMAIL  , new String[]{"dits.delivermybook@gmail.com"});
                    in.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                    in.putExtra(Intent.EXTRA_TEXT   , feedbackstr);
                    in.setPackage("com.google.android.gm");

                    try {
                        startActivity(Intent.createChooser(in, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainActivity.this, "There are no email clients installed.",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        abd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        final AlertDialog ad=abd.create();
        ad.show();

    }


    public void becomeAMemberDialog() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.membership_dialog_layout,null,false);
        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setCancelable(false);
        shopimage =  v.findViewById(R.id.ivShopImage);
        final EditText name = v.findViewById(R.id.name);
        final EditText phone = v.findViewById(R.id.phone);
        final EditText email = v.findViewById(R.id.email);
        final EditText shopName = v.findViewById(R.id.shopName);
        final EditText city = v.findViewById(R.id.city);
        name.setText(Constant.userName);
        phone.setText(Constant.userPhone);
        email.setText(Constant.userEmail);
        city.setText(Constant.cityName);
        shopimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Log.v("-->","Permission is granted");
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, 33);
//                        TedImagePicker.with(MainActivity.this)
//                                .start(new OnSelectedListener() {
//                                    @Override
//                                    public void onSelected(@NotNull Uri uri) {
//                                        CropImage.activity(uri)
//                                                .start(MainActivity.this);
//                                    }
//                                });
                    }
                }
            }
        });
        abd.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(ctx, "send clicked", Toast.LENGTH_SHORT).show();

                if(shopuri==null)
                {
                    Toast.makeText(ctx, "Please select an Image", Toast.LENGTH_SHORT).show();
                }
               else if (TextUtils.isEmpty(name.getText().toString()) &&
                        TextUtils.isEmpty(phone.getText().toString()) &&
                                TextUtils.isEmpty(email.getText().toString()) &&
                                        TextUtils.isEmpty(shopName.getText().toString())
                && TextUtils.isEmpty(city.getText().toString())) {
                    Toast.makeText(MainActivity.this, "fill all fields", Toast.LENGTH_SHORT).show();
                } else {
                    //send vendor_ info to database from here--------------------------
                      pd.show();
                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("image").child(shopuri.getLastPathSegment());

                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), shopuri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    byte[] data = baos.toByteArray();
                    UploadTask uploadTask = filePath.putBytes(data);

                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            shop_url = task.getResult().toString();
                         //   Log.d("fbURL-------->", "onComplete: --->>>"+shop_url);
                            HashMap hm = new HashMap();

                            hm.put("name",name.getText().toString());
                            hm.put("phone",phone.getText().toString());
                            hm.put("shopName",shopName.getText().toString());
                            hm.put("email",email.getText().toString());
                            hm.put("city",city.getText().toString());
                            hm.put("image",shop_url);

                            Log.d("data----->", ""+name.getText().toString()+" "+phone.getText().toString() +shopName.getText().toString()+email.getText().toString()+city.getText().toString());
                            Log.d("URL", "onClick: " + shop_url);
                            addVendor(hm);
                        }
                    });

                }
            }
        });

        abd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        final AlertDialog ad=abd.create();
        ad.show();

    }

    //------prebookDialog---------
    public void preBookDialog() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.prebook_layout,null,false);
        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setCancelable(false);
        final EditText school = v.findViewById(R.id.etSchoolName);
        final EditText phone = v.findViewById(R.id.etPbphone);
        final EditText itemdesc = v.findViewById(R.id.etPbDesc);
        final Spinner spclassname = v.findViewById(R.id.spClass);
        final Spinner spchoice = v.findViewById(R.id.spBookCopySet);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,classNameArr);
        ArrayAdapter ab = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,choiceArr);

        spclassname.setAdapter(aa);
        spchoice.setAdapter(ab);
        final String uid = Constant.uid;
        phone.setText(Constant.userPhone);

        abd.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String classname = spclassname.getSelectedItem().toString().trim();
                final String choice = spchoice.getSelectedItem().toString().trim();
                //Toast.makeText(ctx, "clicked", Toast.LENGTH_SHORT).show();
                 if (TextUtils.isEmpty(school.getText().toString().trim()) ||
                        TextUtils.isEmpty(phone.getText().toString().trim())) {

                    // Log.d("------------->", "inside if--------->");
                    Toast.makeText(MainActivity.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
                }
                 else if (classname.equals("Select") || choice.equals("Select"))
                 {
                     Toast.makeText(ctx, "Please Select class and item Choice", Toast.LENGTH_SHORT).show();
                 }
                 else {
                  //   Log.d("------------->", "inside else--------->");
                   // Sending data to server--------
                     Log.d("-----", "---------->: "+classname +"  "+ choice);
                     HashMap hm = new HashMap();
                     hm.put("school",school.getText().toString().trim());
                     hm.put("itemdesc",itemdesc.getText().toString().trim());
                     hm.put("phone",phone.getText().toString().trim());
                     hm.put("uid",uid);
                     hm.put("classname",classname);
                     hm.put("choice",choice);
                     new Constant().getDataFromServer(MainActivity.this, Request.Method.POST, "addPreBooking", new Constant.OnResponseFromServer() {
                         @Override
                         public void onEvent(String response) {
                             try {
                                 JSONObject obj = new JSONObject(response);
                                 if(obj.getString("status").equals("true"))
                                 {
                                     Toast.makeText(ctx, "Your Request Has Been Submitted", Toast.LENGTH_SHORT).show();
                                 }
                                 else
                                 {
                                     Toast.makeText(ctx, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                 }


                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }


                         }
                     },hm);

                }
            }
        });

        abd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        final AlertDialog ad=abd.create();
        ad.show();

    }

    private void shareApp() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        String subject = "Download this app";
        share.putExtra(Intent.EXTRA_SUBJECT, subject);
        share.putExtra(Intent.EXTRA_TEXT, "Do you want to buy books or stationary item? Just download this amazing app and get your item at your door step. " +"https://play.google.com/store/apps/details?id=" + getPackageName());
        startActivity(Intent.createChooser(share, "Share link!"));
    }

    public void rateThisApp() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void checkUpdate() {
        Constant.updateUrlRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                isUpdateAvailable = dataSnapshot.child("isupdateavailable").getValue().toString();
                mVersionName = dataSnapshot.child("versionname").getValue().toString();

                if (isUpdateAvailable.equals("yes") && !mVersionName.equals(versionName) )
                {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                    alertDialog.setTitle("Update");
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage("update available. Kindly update your app and enjoy new features.");
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {

                            dialog.dismiss();
                            final String appPackageName = getPackageName();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }

                        }
                    });

                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    alertDialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchView.clearFocus();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        new Constant().getDataFromServer(ctx, Request.Method.POST, "getCartItemCount", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Constant.CartItemCount = String.valueOf(Integer.valueOf(jsonArray.getJSONObject(0).getString("count"))+bookSetList.size());
                    badge.setCount(Constant.CartItemCount);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },hm);

    }

    public void onSubCategoryClick(View v) {
        TextView tv = findViewById(v.getId());
        Intent i = new Intent(MainActivity.this,ItemListActivity.class);
        i.putExtra("subcategory",tv.getText().toString());
        i.putExtra("category",tv.getTag().toString());
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        String category="";
        View tv = findViewById(view.getId());

        if (tv.getTag().equals("Uniforms") || tv.getTag().equals("Stationery") || tv.getTag().equals("more")){
            Intent i = new Intent(MainActivity.this,SubCategoryActivity.class);
            if(tv.getTag().toString() == "more") {
                category = "books";
            }
            else
            {
                category = tv.getTag().toString();
            }
            i.putExtra("category",category);
            startActivity(i);
        }else {
            Intent i = new Intent(MainActivity.this,ItemListActivity.class);
            i.putExtra("subcategory","null");
            i.putExtra("category",tv.getTag().toString());
            startActivity(i);
        }


    }

    public void onMoreBtnClick(View v) {
        TextView tv = findViewById(v.getId());
        if (tv.getTag().toString().equals("more_best_seller")){
            Intent i = new Intent(MainActivity.this,ItemListActivity.class);
            i.putExtra("subcategory","Daily Essentials");
            i.putExtra("category","null");
            startActivity(i);
        }else if (tv.getTag().toString().equals("more_new_arrival")){
            Intent i = new Intent(MainActivity.this,ItemListActivity.class);
            i.putExtra("subcategory","New Arrivals");
            i.putExtra("category","null");
            startActivity(i);
        }
    }

    @Override
    public void onItemClick(String shopCode,String shopName) {
        Intent i = new Intent(MainActivity.this,ShopByStoreItemActivity.class);
        i.putExtra("shopName",shopName);
        i.putExtra("shopCode",shopCode);
        startActivity(i);
    }

    public  void addVendor(HashMap hm)
    {
        try {
            new Constant().getDataFromServer(MainActivity.this, Request.Method.POST, "addVendor", new Constant.OnResponseFromServer() {
                @Override
                public void onEvent(String response) {
                    Log.d("vendor response--->", "onEvent: "+response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("status").equals("true"))
                        {
                            Toast.makeText(ctx, "Your Request Has Been Sent For Approval", Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                        else
                        {
                            Toast.makeText(ctx, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pd.dismiss();
                }
            },hm);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                shopuri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(MainActivity.this.getContentResolver(), shopuri);
                    shopimage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }else if (requestCode == 33){
            if (resultCode == Activity.RESULT_OK) {
                CropImage.activity(data.getData())
                        .start(MainActivity.this);
            }
        }
    }

    private void loadHomeScreen(){
        Call<List<HomeInfiniteModel>> call2 = apiInterface.getHomeInfiniteMenu();
        call2.enqueue(new Callback<List<HomeInfiniteModel>>() {
            @Override
            public void onResponse(Call<List<HomeInfiniteModel>> call, Response<List<HomeInfiniteModel>> response) {
                HomeInfiniteAdapter homeInfiniteAdapter = new HomeInfiniteAdapter(response.body(),MainActivity.this,MainActivity.this,apiInterface);
                infiniteList.setAdapter(homeInfiniteAdapter);
//                BestSellerAdapter itemsGridAdapter = new BestSellerAdapter(getApplicationContext(),response.body(),MainActivity.this);
//                bestSellerList.setAdapter(itemsGridAdapter);
//                pd.dismiss();

            }

            @Override
            public void onFailure(Call<List<HomeInfiniteModel>> call, Throwable t) {
                Log.d("----------->",t.getMessage());
            }
        });

    }

    private void loadBestSeller(){
        Call<List<ItemsGridModel>> call2 = apiInterface.getBestSeller(Constant.searchCityName);
        call2.enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, Response<List<ItemsGridModel>> response) {
                BestSellerAdapter itemsGridAdapter = new BestSellerAdapter(getApplicationContext(),response.body(),MainActivity.this);
                bestSellerList.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {

            }
        });

    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("-->","Permission is granted");
                return true;
            } else {

                Log.v("-->","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("-->","Permission is granted");
            return true;
        }
    }

    private void loadNewArrival(){
        Call<List<ItemsGridModel>> call2 = apiInterface.getNewArrival(Constant.searchCityName);
        call2.enqueue(new Callback<List<ItemsGridModel>>() {
            @Override
            public void onResponse(Call<List<ItemsGridModel>> call, Response<List<ItemsGridModel>> response) {
                newArrivalAdapter itemsGridAdapter = new newArrivalAdapter(getApplicationContext(),response.body(),MainActivity.this);
                newArrivalList.setAdapter(itemsGridAdapter);
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<List<ItemsGridModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(String itemId) {
        pd.show();
        Constant c = new Constant();
        HashMap hm = new HashMap();
        hm.put("uid",Constant.uid);
        hm.put("itemId",itemId);
        hm.put("quantity","1");
        hm.put("size","-");

        c.getDataFromServer(getApplicationContext(), Request.Method.POST, "addToCart", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                pd.dismiss();
                if (!response.equals("error")){
                    onResume();
                    Toast.makeText(MainActivity.this, "Added", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

            }
        },hm);
    }

    @Override
    public void onItemClick(HomeMenuModel item) {
        Intent i = new Intent(MainActivity.this,SubCategoryActivity.class);
        i.putExtra("category",item.text);
        startActivity(i);
    }

    @Override
    public void onBannerClick(String category) {
        Intent i = new Intent(MainActivity.this,ItemListActivity.class);
        i.putExtra("subcategory","null");
        i.putExtra("category",category);
        startActivity(i);
    }
}
