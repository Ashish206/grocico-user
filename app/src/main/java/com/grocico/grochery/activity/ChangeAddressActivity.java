package com.grocico.grochery.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grocico.grochery.R;
import com.grocico.grochery.extra.Constant;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ChangeAddressActivity extends AppCompatActivity {
    Button getAddressBtn;
    private static final int MY_PERMISSION_REQUEST_CODE = 7192;

    protected static final int REQUEST_CHECK_SETTINGS = 2;
    private LocationRequest mLocationRequest;
    double latitude = 0.0,longitude=0.0;
    String addressString = "";
    String cityString = "";
    String stateString = "";
    String countryString = "";
    String postalCodeString = "";
    String completeAddress = "";


    TextInputEditText email;
    TextInputEditText name;
    TextInputEditText phone;
    TextInputEditText postalCode;
    TextInputEditText city;
    TextInputEditText address;
    TextInputEditText state;
    TextInputEditText landMark;
    TextView saveTxt;
    ProgressDialog progressDialog;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;

    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private Boolean mRequestingLocationUpdates;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_address);
        email = findViewById(R.id.emailField);
        name = findViewById(R.id.nameField);
        phone = findViewById(R.id.phoneField);
        postalCode = findViewById(R.id.postalcodeField);
        city = findViewById(R.id.cityField);
        state = findViewById(R.id.stateField);
        address = findViewById(R.id.addressField);
        landMark = findViewById(R.id.landMarkField);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sp = getSharedPreferences("admin",MODE_PRIVATE);
        editor = sp.edit();


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        saveTxt = findViewById(R.id.save_btn);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        email.setText(Constant.userEmail);
        name.setText(Constant.userName);
        phone.setText(Constant.userPhone);
        postalCode.setText(Constant.postalCode);
        city.setText(Constant.cityName);
        landMark.setText(Constant.landMark);
        address.setText(Constant.userAddress);
        state.setText(Constant.stateName);
        displayLocationSettingsRequest(this);

        getAddressBtn = findViewById(R.id.getAddressBtn);
        getAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                startLocationUpdates();


            }
        });

        postalCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                   // progressDialog.show();
//                    Constant.rootRef.child("DeliveryCharge").child(postalCode.getText().toString()).addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.hasChild("charge")){
//                                Constant.deliveryCharge = dataSnapshot.child("charge").getValue().toString();
//                                city.setText(dataSnapshot.child("district").getValue().toString());
//                                progressDialog.dismiss();
//                            }else {
//                                progressDialog.dismiss();
//                                postalCode.setError("We do not deliver our items to this pin code. please try another nearest one.");
//                                postalCode.requestFocus();
//                                postalCode.setText("");
//                            }
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
                }else{
                    //Toast.makeText(this, "Get Focus", Toast.LENGTH_SHORT).show();
                }

            }
        });

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(email.getText().toString())){
                    email.setError("enter email");
                    email.requestFocus();
                }
                else if (TextUtils.isEmpty(name.getText().toString())){
                    name.setError("enter name");
                    name.requestFocus();
                }
                else if (TextUtils.isEmpty(phone.getText().toString())){
                    phone.setError("enter phone");
                    phone.requestFocus();
                }
                else if (TextUtils.isEmpty(postalCode.getText().toString())){
                    postalCode.setError("enter postal code");
                    postalCode.requestFocus();
                }
                else if (TextUtils.isEmpty(city.getText().toString())){
                    city.setError("enter city");
                    city.requestFocus();
                }
                else if (TextUtils.isEmpty(state.getText().toString())){
                    state.setError("enter state");
                    state.requestFocus();
                }
                else if (TextUtils.isEmpty(address.getText().toString())){
                    address.setError("enter address");
                    address.requestFocus();
                }
                else if (TextUtils.isEmpty(landMark.getText().toString())){
                    landMark.setError("enter landmark");
                    landMark.requestFocus();
                }
                else {

//                    Constant.deliveryCharge = dataSnapshot.child("charge").getValue().toString();
//                    city.setText(dataSnapshot.child("district").getValue().toString());
                    progressDialog.show();
                    editor.putString("email",email.getText().toString());
                    editor.putString("userName",name.getText().toString());
                    editor.putString("city",city.getText().toString());
                    editor.putString("state",state.getText().toString());
                    editor.putString("phone", phone.getText().toString());
                    editor.putString("address",address.getText().toString());
                    editor.putString("landmark",landMark.getText().toString());
                    editor.putString("postalCode",postalCode.getText().toString());
                    Constant.userEmail = email.getText().toString();
                    Constant.userName = name.getText().toString();
                    Constant.userPhone = phone.getText().toString();
                    Constant.cityName = city.getText().toString().toLowerCase();
                    Constant.stateName = state.getText().toString().toLowerCase();
                    Constant.postalCode = postalCode.getText().toString().toLowerCase();
                    Constant.landMark = landMark.getText().toString().toLowerCase();
                    Constant.userAddress = address.getText().toString().toLowerCase();


                    try {
                        editor.putString("lat",String.valueOf(latitude));
                        editor.putString("lng",String.valueOf(longitude));
                        Constant.userLat = String.valueOf(latitude);
                        Constant.userLng = String.valueOf(longitude);
                    }catch (Exception e){
                        editor.putString("lat",String.valueOf(0.0));
                        editor.putString("lng",String.valueOf(0.0));
                        Constant.userLat = String.valueOf(0.0);
                        Constant.userLng = String.valueOf(0.0);
                    }
                    editor.apply();

                    RequestQueue queue = Volley.newRequestQueue(ChangeAddressActivity.this);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.BASE_URL + "addUserAddressDetails", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try{
                                JSONObject job = new JSONObject(response);
                                if (job.getString("status").equals("1")){

                                    finish();
                                    Toast.makeText(ChangeAddressActivity.this, "Address Changed", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(ChangeAddressActivity.this, "Opps!! "+job.getString("message"), Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }

                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                            progressDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Log.d("RESPONSE:>>> ", error.toString() + "<<<");
                            Toast.makeText(ChangeAddressActivity.this, ")pps !! " + error.toString(), Toast.LENGTH_SHORT).show();

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> jsonParams = new ArrayMap<>();
                            jsonParams.put("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                            jsonParams.put("email", email.getText().toString().toLowerCase());
                            jsonParams.put("name", name.getText().toString().toLowerCase());
                            jsonParams.put("phone", phone.getText().toString());
                            jsonParams.put("city", Constant.cityName);
                            jsonParams.put("state", Constant.stateName);
                            jsonParams.put("landmark", Constant.landMark);
                            jsonParams.put("address", Constant.userAddress);
                            jsonParams.put("lat", Constant.userLat);
                            jsonParams.put("lng", Constant.userLng);
                            jsonParams.put("postalCode", Constant.postalCode);
                            return jsonParams;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/x-www-form-urlencoded";
                        }
                    };
                    queue.add(stringRequest);
                }
            }
        });


        init();
        restoreValuesFromBundle(savedInstanceState);
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            getAddress(this, mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());

        }


    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        //Log.i(TAG, "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                // Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                //   "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(ChangeAddressActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    // Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                // Log.e(TAG, errorMessage);

                                Toast.makeText(ChangeAddressActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        displayLocationSettingsRequest(getApplicationContext());//keep asking if imp or do whatever
                        break;
                }
                break;

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRequestingLocationUpdates) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 &&grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {

                }
        }
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(ChangeAddressActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            //Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void getAddress(Context context, double LATITUDE, double LONGITUDE) {
        //Set Address
        try {

            progressDialog.dismiss();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                latitude = LATITUDE;
                longitude = LONGITUDE;
                cityString = addresses.get(0).getLocality();
                stateString = addresses.get(0).getAdminArea();
                countryString = addresses.get(0).getCountryName();
                postalCodeString = addresses.get(0).getPostalCode();
                completeAddress  = addresses.get(0).getAddressLine(0);

                postalCode.setText(postalCodeString);
                city.setText(cityString);
                address.setText(completeAddress);
                state.setText(stateString);

                try{
                    addressString = completeAddress.substring(0, completeAddress.indexOf(", "+cityString));
                }catch (Exception e){
                    e.printStackTrace();
                }

                stopLocationUpdates();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
